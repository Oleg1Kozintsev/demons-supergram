<?php
/**
 * Created by PhpStorm.
 * User: okozi
 * Date: 15.08.2018
 * Time: 20:11
 */
namespace demonsThebloggers\Server;

use Medoo\Medoo;
use demonsThebloggers\Actions\postAnalyzer;
use demonsThebloggers\Actions\winnerCalc;
use React\EventLoop\Timer\TimerInterface;

class RealtimeHttpServer
{
    const HOST = '127.0.0.1';
    const PORT = 1307;
    const TIMEOUT = 5;
    const TOKEN =  '43147513-f292-47c7-aa2a-de3ca3a858cf';
    /** @var \React\Promise\Deferred[] */
    protected $_contexts;
    /** @var \React\EventLoop\LoopInterface */
    protected $_loop;
    /** @var \InstagramAPI\Instagram */
    protected $_instagram;
    /** @var \React\Http\Server */
    protected $_server;
    /** @var \Psr\Log\LoggerInterface */
    protected $_logger;
    /** @var Medoo */
    protected $_database;
    /** @var TimerInterface */
    protected $_timer;
    /** @var bool  */
    protected $_isError;
    /** @var string */
    protected $_errorMsg;


    /**
     * Constructor.
     *
     * @param \React\EventLoop\LoopInterface $loop
     * @param \InstagramAPI\Instagram        $instagram
     * @param Medoo $database
     * @param \Psr\Log\LoggerInterface|null  $logger
     */
    public function __construct(
        \React\EventLoop\LoopInterface $loop,
        \InstagramAPI\Instagram $instagram,
        Medoo $database,
        \Psr\Log\LoggerInterface $logger = null)
    {
        $this->_loop = $loop;
        $this->_instagram = $instagram;
        $this->_database = $database;
        if ($logger === null) {
            $logger = new \Psr\Log\NullLogger();
        }
        $this->_logger = $logger;
        $this->_contexts = [];
        $this->_isError = false;
        $this->_startHttpServer();
        $this->_startTimer();
    }
    /**
     * Gracefully stop everything.
     */
    protected function _stop()
    {
        // Initiate shutdown sequence.
        // Wait 2 seconds for Realtime to shutdown.
        $this->_loop->addTimer(2, function () {
            $this->_loop->cancelTimer($this->_timer);
            // Stop main loop.
            $this->_loop->stop();
        });
    }

    /**
     * Handler for incoming HTTP requests.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     *
     * @return \React\Http\Response|\React\Promise\PromiseInterface
     */
    public function onHttpRequest(
        \Psr\Http\Message\ServerRequestInterface $request)
    {
        // Treat request path as command.
        $command = $request->getUri()->getPath();
        // Params validation is up to you.
        $params = $request->getQueryParams();
        // Log command with its params.
        //$this->_logger->debug(sprintf('Received command %s', $command), $params);
        $headers = [
            'Content-Type' => 'text/plain',
            'Access-Control-Allow-Origin' => '*',
        ];
        switch ($command) {
            case '/ping':
                return new \React\Http\Response(200, $headers, 'pong');
            case '/stop':
                $context = $params['token'];
                if ($context === false || $context !== self::TOKEN) {
                    return new \React\Http\Response(503);
                }
                $this->_stop();
                return new \React\Http\Response(200, $headers, 'stop');
            case '/status':
                return new \React\Http\Response($this->_isError === false ? 200 : 503, $headers, 'status');
            case '/go':
                $context = $params['token'];
                if ($context === false || $context !== self::TOKEN) {
                    return new \React\Http\Response(503, $headers);
                }
                $this->_logger->debug('startPostAnalyzer');
                //$this->startPostAnalyzer();
                return new \React\Http\Response(200, $headers);
            default:
                $this->_logger->warning(sprintf('Unknown command %s', $command), $params);
                // If command is unknown, reply with 404 Not Found.
                return new \React\Http\Response(404, $headers);
        }
    }

    protected function finishPostAnalyzer()
    {
        $where = [
            "is_sync" => 1,
            "is_error" => 0,
            "paid" => 1
        ];

        $rows_count = $this->_database->count("new_post", $where);

        if ($rows_count == 0) {
            //echo "Rows count = 0 \n";
            return;
        }

        $this->_logger->debug("finishPostAnalyzer start Rows count <> 0");

        $rows = $this->_database->select("new_post", [
            "id", "code", "valid_until", "type", "del_comment"
        ], $where );

        $analyzer = new postAnalyzer($this->_database, $this->_instagram, $this->_logger);

        foreach ($rows as $item) {
            if ($status = $analyzer->getPostInfoFromDb($item['code'])){
                $analyzer->getComments();

                $row = [
                    "status" => 8
                ];

                $this->_database->update("new_post", $row, [
                    "id" => $item['id']
                ]);
            } else {
                $this->_logger->error("finishPostAnalyzer getPostInfoFromDb error");
            }
        }
    }

    protected function startPostAnalyzer()
    {
        $where = [
            "is_sync" => 0,
            "is_error" => 0,
            "paid" => 0
        ];

        $rows_count = $this->_database->count("new_post", $where);

        if ($rows_count == 0) {
            //echo "Rows count = 0 \n";
            return;
        }

        $rows = $this->_database->select("new_post", [
            "id", "code", "valid_until", "type", "del_comment"
        ], $where );

        $analyzer = new postAnalyzer($this->_database, $this->_instagram, $this->_logger);
        $winnerCalc = new winnerCalc($this->_database, $this->_logger);

        foreach ($rows as $item) {
            if ($status = $analyzer->getPostInfo($item['code'], $item['id'])){
                $row = [
                    "is_sync" => 1,
                    "is_error" => 0,
                    "status" => $analyzer->getStatus()
                ];

            } else {
                $row = [
                    "is_sync" => 1,
                    "is_error" => 1,
                    "status" => $analyzer->getStatus()
                ];
            }
            $this->_database->update("new_post", $row, [
                "id" => $item['id']
            ]);

            if ($status){
                $analyzer->getComments();
                if ($winnerCalc->GetWinnerId($analyzer->getPostId(), $item['valid_until'], $item['type']) ===0){
                    $this->_logger->error("The winner is not determined. PostId:" . $analyzer->getPostId());
                    $row = [
                        "is_sync" => 1,
                        "is_error" => 1,
                        "status" => 7
                    ];
                } else {
                    $row = [
                        "is_complited" => 1
                    ];
                }

                $this->_database->update("new_post", $row, [
                    "id" => $item['id']
                ]);
            }
            if ($item['del_comment'] === 1) {
                $this->_database->delete("comments", [
                    'post_id' => $analyzer->getPostId()
                ]);
            }
        }
    }

    protected function _startTimer()
    {
        $this->_timer = $this->_loop->addPeriodicTimer(10, function () {
            if ($this->_isError){
                return;
            }
            try{
                $this->startPostAnalyzer();
                //todo: продолжение ниже, для оплаченного получения csv файла
                $this->finishPostAnalyzer();
            } catch (\Exception $ex) {
                $this->_isError = true;
                $this->_errorMsg = $ex->getMessage();
            }
        });
    }

    /**
     * Init and start HTTP server.
     */
    protected function _startHttpServer()
    {
        // Create server socket.
        $socket = new \React\Socket\Server(self::HOST.':'.self::PORT, $this->_loop);
        $this->_logger->info(sprintf('Listening on http://%s', $socket->getAddress()));
        // Bind HTTP server on server socket.
        $this->_server = new \React\Http\Server([$this, 'onHttpRequest']);
        $this->_server->listen($socket);
    }
}