#!/usr/bin/php
<?php
/**
 * @author Oleg Kozintsev
 * @link
 * путь к скрипту на сервере:
 * php /var/www/user59421/data/demon/schedulerService.php
 */
//Игнорировать обрыв связи с браузером
ignore_user_abort(1);
//Время работы скрипта неограничено
set_time_limit(0);
date_default_timezone_set('UTC');

require_once("vendor/autoload.php");
require_once("config/db.php");
require_once("config/main.php");

use Medoo\Medoo;
use demonsThebloggers\Models\Proxy;
use demonsThebloggers\Actions\UploadPhoto;


$database = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $db_name,
    'server' => 'localhost',
    'username' => $db_user,
    'password' => $db_pass,
    'charset' => 'utf8',
]);

$cron = True;

function RunTasks(Medoo $database, $photo_folder)
{
    $date = new DateTime();
    $timestampe = $date->getTimestamp();

    $start_date = $timestampe - 70;
    $end_date = $timestampe + 70;

    // переменный для тестов
    //$start_date = 1496304000;
    //$end_date = 1496308500;

    try {
        $sql = <<<SQL
SELECT task.id, task_time, task, username, password, casename, idproxy FROM task, task_case, instagram
WHERE task_time BETWEEN :start_date AND :end_date
AND launched = '0' AND task.id_case = task_case.id_case AND instagram.id = task.id_instagram
SQL;
        $rows = $database->query(
            $sql, [
                ":start_date" => $start_date,
                ":end_date" => $end_date,
            ]
        )->fetchAll();
        $c = count($rows);
        if ($c == 0){
            echo "Rows count = 0 \n";
            return;
        }
        //$pool = new Pool($c);
        foreach ($rows as $row) {
            print_r($row);

            $database->insert("service_log", [
                "message" => "task start",
            ]);


            $id_task = $row['id'];
            $task = json_decode($row['task'], true); // задача в формате JSON
            $username = $row['username'];
            $password = $row['password'];
            $casename = $row['casename']; // название задачи
            $idproxy = $row['idproxy'];
            $proxy_rows = $database->select("proxy", [
                "iporhost",
                "port",
                "login",
                "password",
                "host_type",
            ], [
                "idproxy" => $idproxy
            ]);
            $proxy = null;
            foreach ($proxy_rows as $item) {
                $proxy = new Proxy();
                $proxy->setIporhost($item['iporhost']);
                $proxy->setPort($item['port']);
                $proxy->setLogin($item['login']);
                $proxy->setPassword($item['password']);
                $proxy->setType($item['host_type']);
                break; // one by one
            }
            if ($casename == "upload_image") {
                $database->update("task", [
                    "launched" => "1",
                ], [
                    "id" => $id_task
                ]);

                $photo = $photo_folder . $task['photo'];
                $caption = $task['caption'];
                $id_task = $row['id'];

                $uploadPhoto = new UploadPhoto($username, $password, $photo, $caption);
                if (isset($proxy))
                    $uploadPhoto->setProxy($proxy->getUrl());
                $uploadPhoto->run();
                if ($uploadPhoto->isError()) {
                    $msg = $uploadPhoto->getMessage();
                    print "Error!: " . $msg;
                    $database->insert("service_log", [
                        "message" => $msg,
                    ]);
                } else {
                    $database->update("task", [
                        "сompleted" => "1",
                    ], [
                        "id" => $id_task
                    ]);
                }
                //$pool->submit($uploadPhoto);
            }
        }
        // while ($pool->collect(function ($uploadPhoto) {
        //     return $uploadPhoto->isCompleted();
        // })) continue;
        // $pool->shutdown();
        //todo: подумать как сделать отметку о завершении задачи, т.к. это не так просто для задач выполняемых в отдельном потоке
        //$dbh->query("INSERT INTO `supergram`.`service_log` (`message`) VALUES ('Script stop.')");

    } catch (Exception $e) {
        print "Exception!: " . $e->getMessage();
        $msg = $e->getMessage();
        $database->insert("service_log", [
            "message" => $msg,
        ]);
        exit(1);
    }
}

if ($cron) {
    echo "Script run! \n";
    RunTasks($database, $photo_folder);
} else {
    echo "Demon run! \n";
    // Чтобы программа работала постоянно, она просто должна постоянно работать ;)
    while (1) {
        // Тут будет располагаться код Демона
        RunTasks($database, $photo_folder);
        // Время сна Демона между итерациями (зависит от потребностей системы)
        sleep(10);
    }
    print "Exit";
    exit(1);
}




