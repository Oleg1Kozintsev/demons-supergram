<?php
/**
 * @author Oleg Kozintsev
 * @link
 * скрипт для анализа пользователей в инстаграмме
 */
//Игнорировать обрыв связи с браузером
ignore_user_abort(1);
//Время работы скрипта неограничено
set_time_limit(0);

require_once __DIR__ . "/vendor/autoload.php";
require_once __DIR__ . "/config/db.php";

require_once("config/user-oleg.php");

use Medoo\Medoo;
use kozintsev\ALogger\Logger;
use demonsThebloggers\Models\Proxy;
use demonsThebloggers\Actions\accountAnalyzer;

// Initialize
$database = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $db_name,
    'server' => $db_server,
    'username' => $db_user,
    'password' => $db_pass,
    'charset' => 'utf8',
]);

$c = $database->count("invite");
echo "BD test. Ok. C (invite table) = " . $c;

$log_file = __DIR__ . '/log/comment-grabber-one.log';
$logger = new Logger($log_file, \Psr\Log\LogLevel::DEBUG);

$ig = new \InstagramAPI\Instagram(false, false);
try {
    if (isset($proxyCfg) && $proxyCfg != []) {
        $proxy = new Proxy();
        $proxy->config($proxyCfg);
        $ig->setProxy($proxy->getUrl());
    }
    $ig->login($username, $password);
} catch (\Exception $e) {
    echo 'Something went wrong: ' . $e->getMessage() . "\n";
    $logger->error('Login... Something went wrong: ' . $e->getMessage());
    return;
}

//$username = 'worldofbeautyful';
//$username = 'tetyamotya';
$username = 'buzova86';
//$username = 'lenamyro';
//$username = 'astistudio_';

echo "Get comment by " . $username . "\n";

$logger->info("Start comment grabber username:" . $username);

$a = new accountAnalyzer($database, $ig, ['username' => $username], $log_file);
// указать media ID
$mediaId = "1645616125600321889";
$is_error = false;

if (empty($a->getPk())) {
    $logger->error("Instagram user id = 0");
    exit(0);
}

$logger->info("Loop 2 run");

if ($a->collectionOfComments($mediaId) === false) {
    // ошибка может быть потому что посту удалилил например
    $logger->error($a->getMessage());
    $is_error = true;
}
echo "Exit " . $username . ". Finish! \n";


