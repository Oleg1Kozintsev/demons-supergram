<?php
//Игнорировать обрыв связи с браузером
ignore_user_abort(1);
//Время работы скрипта неограничено
set_time_limit(0);

require_once("vendor/autoload.php");
require_once("config/db.php");

use Medoo\Medoo;
use kozintsev\ALogger\Logger;
use demonsThebloggers\Actions\updateCache;

$log_file = __DIR__ . '/log/update-cache.log';

$logger = new Logger($log_file, \Psr\Log\LogLevel::INFO);

$database = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $db_name,
    'server' => $db_server,
    'username' => $db_user,
    'password' => $db_pass,
    'charset' => 'utf8'
]);

$where = [
    "is_sync" => 1,
    "is_error" => 0,
    "is_do" => 1,
    "is_404" => 0
];

$rows_count = $database->count("stats_accounts", $where);

if ($rows_count == 0) {
    echo "Rows count = 0 \n";
    return;
}

echo "Start! Accounts: " . $rows_count . "\n";

$logger->info("Start! Accounts: " . $rows_count);

updateCache::updateCacheStatsAccounts($database, $logger);

updateCache::updateCacheTopFollowers($database, 1153);

$logger->info("Finish!");
