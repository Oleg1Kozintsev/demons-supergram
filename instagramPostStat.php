<?php
/**
 * @author Oleg Kozintsev
 * @link
 * скрипт для анализа пользователей в инстаграмме
 */
//Игнорировать обрыв связи с браузером
ignore_user_abort(1);
//Время работы скрипта неограничено
set_time_limit(0);
date_default_timezone_set('UTC');

require_once ("vendor/autoload.php");
require_once ("config/db.php");
require_once ("config/main.php");

use demonsThebloggers\Actions\accountAnalyzer;
use demonsThebloggers\Models\Proxy;
use Medoo\Medoo;
use kozintsev\Alogger\Logger;

$cron = True;

// Initialize
$database = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $db_name,
    'server' => 'localhost',
    'username' => $db_user,
    'password' => $db_pass,
    'charset' => 'utf8',
]);


/*
 * Что бы анализировать аккаунт нужно добавить его через UI в базу данных
 */
function RunTasks($username, $password, $proxyCfg, Medoo $database, $is_sync, $log_file){
    $logger = new Logger($log_file, \Psr\Log\LogLevel::DEBUG);
    if ($is_sync == 0) {
        $arr = [
            "is_sync" => $is_sync,
            'is_error' => 0,
            "is_do" => 1
        ];
    } else {
        $arr = [
            "is_sync" => $is_sync,
            "is_error" => 0,
            "is_do" => 1
        ];
    }
    $rows = $database->select("stats_accounts", [
        "username",
    ], $arr );

    if (count($rows) == 0) {
        echo "Rows count = 0 \n";
        return;
    }

    $ig = new \InstagramAPI\Instagram(true, false);
    try {
        if (isset($proxyCfg) && $proxyCfg != [])
        {
            $proxy = new Proxy();
            $proxy->config($proxyCfg);
            $ig->setProxy($proxy->getUrl());
        }
        $ig->login($username, $password);
    } catch (\Exception $e) {
        echo 'Something went wrong: '.$e->getMessage()."\n";
        $logger->error('Something went wrong: '.$e->getMessage());
        return;
    }

    foreach ($rows as $item) {
        $a = new accountAnalyzer($database, $ig, $item, $log_file);
        if ($a->GetPostsInfo(False, True) == false){
            $logger->error($a->getMessage());
        }
        //break; //- fot faster tests
        sleep(3);
    }
}


if ($cron) {
    echo "Script run! \n";
    RunTasks($username, $password, $proxyCfg, $database, $is_sync, $log_file);
    echo "Finish! \n";
} else {
     echo "Demon run! \n";
    // Чтобы программа работала постоянно, она просто должна постоянно работать ;)
    while (1) {
        // Тут будет располагаться код Демона
        RunTasks($username, $password, $proxyCfg, $database, $is_sync, $log_file);
        // Время сна Демона между итерациями (зависит от потребностей системы)
        sleep(10);
    }
    print "Exit";
    exit(0);
}