<?php
class Logger
{
    private $flogname;
    private $need_info;

    public function __construct($filename, $need_info = true)
    {
        $this->flogname = $filename;
        $this->need_info = $need_info;
    }
    /*
     * @param string $s
     * @param boolean $usedate
     */
    private function write($s, $usedate = true) {
		// пишем в лог-файл строку $s,
		try {
			// $usedate - вставлять ли в лог дату/время текущие
			$fplog = fopen($this->flogname, 'ab');
			if($usedate)
				$tim = '['.date('Y-m-d H:i:s').'] ';
			else
				$tim = '';
			fwrite($fplog, $tim.$s."\n");
			fclose($fplog);
		} catch (Exception $e) {
    		echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
    		$fplog = null;
		}
	}
    /*
     * @param string $message
     */
	public function info($message){
        if ($this->need_info)
	        $this->write('INFO: ' . $message);
    }
    /*
     * @param string $message
     */
    public function error($message){
        $this->write('ERROR: ' . $message);
    }
}