<?php

require_once __DIR__. '/../vendor/autoload.php';
require_once __DIR__. '/../config/db.php';
require_once __DIR__. '/../config/main.php';

use Medoo\Medoo;

$database = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $db_name,
    'server'   => $db_server,
    'username' => $db_user,
    'password' => $db_pass,
    'charset' => 'utf8',
]);


try {
    // $usedate - вставлять ли в лог дату/время текущие
    $handle = fopen(__DIR__. "/order_2833951.txt", 'r+');
    while (!feof($handle)) {
        $buffer = fgets($handle, 4096);
        $array = explode(":", $buffer);
        $username = $array[0];
        $pass = $array[1];
        $mail = $array[2];
        $mail_pass = $array[3];
        $date = new DateTime();
        $timestamp = $date->getTimestamp();
        $count = $database->count('bot', ['username' => $username]);
        if ($count === 0){
            $database->insert('bot', [
                'username' => $username,
                'password' => $pass,
                'email' => $mail,
                'email_pass' => $mail_pass,
                'created_at' => $timestamp
            ]);
        } else {
            $database->update('bot', [
                'password' => $pass,
                'email' => $mail,
                'email_pass' => $mail_pass,
                'updated_at' => $timestamp
            ], [
                'username' => $username,
            ]);
        }

        echo $buffer;
    }
    fclose($handle);
} catch (Exception $e) {
    echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
    $handle = null;
}