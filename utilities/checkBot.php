<?php

require_once __DIR__. '/../vendor/autoload.php';
require_once __DIR__. '/../config/db.php';
require_once __DIR__. '/../config/main.php';

use Medoo\Medoo;

$database = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $db_name,
    'server'   => $db_server,
    'username' => $db_user,
    'password' => $db_pass,
    'charset' => 'utf8',
]);


$rows = $database->select('bot', ['id', 'username',  'password', 'email', 'email_pass'],
    ['is_check' => 0]);

foreach ($rows as $item) {
    $id = $item['id'];
    $username = $item['username'];
    $password = $item['password'];

    $date = new DateTime();
    $timestamp = $date->getTimestamp();

    $ig = new \InstagramAPI\Instagram(true, false);
    try {
//        if (isset($proxyCfg) && $proxyCfg != [])
//        {
//            $proxy = new \demons\models\Proxy();
//            $proxy->config($proxyCfg);
//            $ig->setProxy($proxy->getUrl());
//        }
        $ig->login($username, $password);
    } catch (\Exception $e) {
        echo 'Something went wrong: '.$e->getMessage()."\n";
        $database->update('bot', [
            'is_check' => 1,
            'is_login' => 0,
            'updated_at' => $timestamp
        ], [
            'id' => $id,
        ]);
        sleep(120);
        continue;
    }

    $database->update('bot', [
        'is_check' => 1,
        'is_login' => 1,
        'updated_at' => $timestamp
    ], [
        'id' => $id,
    ]);

    sleep(120);
}