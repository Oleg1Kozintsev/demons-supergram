<?php
/**
 * @author Oleg Kozintsev
 * @link
 * скрипт для анализа пользователей в инстаграмме
 */
//Игнорировать обрыв связи с браузером
ignore_user_abort(1);
//Время работы скрипта неограничено
set_time_limit(0);
date_default_timezone_set('UTC');

require_once("vendor/autoload.php");
require_once("config/db.php");
require_once("config/main.php");

$is_sync = 1;
$is_like_stat = False;
$is_data_check = True;

require_once("config/user-dunsolomon.php");

use Medoo\Medoo;
use kozintsev\ALogger\Logger;
use demonsThebloggers\Models\Proxy;
use demonsThebloggers\Models\MaxAccount;
use demonsThebloggers\Actions\accountAnalyzer;

$cron = True;

// Initialize
$database = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $db_name,
    'server' => $db_server,
    'username' => $db_user,
    'password' => $db_pass,
    'charset' => 'utf8',
]);


/*
 * Что бы анализировать аккаунт нужно добавить его через UI в базу данных
 */
function RunTasks($username, $password, $proxyCfg, Medoo $database, $log_file)
{
    $logger = new Logger($log_file, \Psr\Log\LogLevel::DEBUG);

    $ig = new \InstagramAPI\Instagram(true, false);
    try {
        if (isset($proxyCfg) && $proxyCfg != []) {
            $proxy = new Proxy();
            $proxy->config($proxyCfg);
            $ig->setProxy($proxy->getUrl());
        }
        $ig->login($username, $password);
    } catch (\Exception $e) {
        echo 'Something went wrong: ' . $e->getMessage() . "\n";
        $logger->error('Something went wrong: ' . $e->getMessage());
        return;
    }

    MaxAccount::$max_followers_count = $database->max('stats_accounts','followers_count');
    MaxAccount::$max_er = $database->get('stats_accounts', 'er', ['followers_count' => MaxAccount::$max_followers_count]);

    //$username = ['iren.shm'];
    $username = [
        'username'=>'buzova86'
    ];

    $a = new accountAnalyzer($database, $ig, $username, $log_file);
    if ($a->GetAccountInfo(False, True, True) == false) {
        $logger->error($a->getMessage());
    }
}


if ($cron) {
    echo "Script run! \n";
    RunTasks($username, $password, $proxyCfg, $database, $log_file);
    echo "Finish! \n";
} else {
    echo "Demon run! \n";
    // Чтобы программа работала постоянно, она просто должна постоянно работать ;)
    while (1) {
        // Тут будет располагаться код Демона
        RunTasks($username, $password, $proxyCfg, $database, $log_file);
        // Время сна Демона между итерациями (зависит от потребностей системы)
        sleep(10);
    }
    print "Exit";
    exit(0);
}