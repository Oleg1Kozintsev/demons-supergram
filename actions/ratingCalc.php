<?php
namespace demonsThebloggers\Actions;

use demonsThebloggers\Models\MaxAccount;
use demonsThebloggers\Models\Post;
use Exception;

class ratingCalc
{
    private $like_avg;
    private $comment_avg;
    private $rating;
    private $er;
    private $lr;
    private $tr;
    private $followers_count;
    private $max_followers_count;
    private $max_er;
    private $following_count;
    private $media_count;
    private $arrLike = [];
    private $arrComment = [];

    /**
     * ratingCalc constructor.
     * @param $top
     * @param int $followers_count
     * @param int $following_count
     * @param int $media_count
     * @throws Exception
     * @internal param $array [Post] $top
     */
    public function __construct($top, $followers_count, $following_count, $media_count)
    {
        $this->followers_count = $followers_count;
        $this->following_count = $following_count;
        $this->media_count = $media_count;
        if (isset($top) && is_array($top)){
            foreach ($top as $item) {
                if($item instanceof Post) {
                    $this->arrLike [] = $item->like_count;
                    $this->arrComment [] = $item->comment_count;
                } else {
                    throw new Exception("Array top not implementation class Post");
                }
            }
        } else {
            $this->arrLike [] = 0;
            $this->arrComment [] = 0;
        }
        $this->max_followers_count = MaxAccount::$max_followers_count;
        $this->max_er = MaxAccount::$max_er;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        $case = true;
        if ($case){
            $this->ratingCase1();
        } else {
            $this->ratingCase2();
        }
        return $this->rating;
    }

    /**
     * @return mixed
     */
    public function getEr()
    {
        if (empty($this->like_avg)) {
            $this->like_avg = $this->getLikeAvg();
        }
        if (empty($this->comment_avg)) {
            $this->comment_avg = $this->getCommentAvg();
        }
        $sum = $this->like_avg + $this->comment_avg;
        if ($this->followers_count > 0) {
            if ($sum > $this->followers_count) {
                $this->er = 100;
            } else {
                $this->er = round(($sum / $this->followers_count) * 100, 2);
            }
        } else {
            $this->er = 0;
        }
        return $this->er;
    }

    /**
     * @return mixed
     */
    public function getLr()
    {
        if (empty($this->like_avg)) {
            $this->like_avg = $this->getLikeAvg();
        }
        if ($this->followers_count > 0) {
            if ($this->like_avg > $this->followers_count) {
                $this->lr = 100;
            } else {
                $this->lr = round(($this->like_avg / $this->followers_count) * 100, 2);
            }
        } else {
            $this->lr = 0;
        }
        return $this->lr;
    }

    /**
     * @return mixed
     */
    public function getTr()
    {
        if (empty($this->comment_avg)) {
            $this->comment_avg = $this->getCommentAvg();
        }
        if ($this->followers_count > 0) {
            if ($this->comment_avg > $this->followers_count) {
                $this->tr = 100;
            } else {
                $this->tr = round(($this->comment_avg / $this->followers_count) * 100, 2);
            }
        } else {
            $this->tr = 0;
        }
        return $this->tr;
    }

    /**
     * @return int
     */
    public function getLikeAvg()
    {
        $c = count($this->arrLike);
        if ($c == 0) {
            $this->like_avg = 0;
        } else {
            $this->like_avg = $this->median($this->arrLike);
        }
        return $this->like_avg;
    }

    /**
     * @return int
     */
    public function getCommentAvg()
    {
        $c = count($this->arrComment);
        if ($c == 0) {
            $this->comment_avg = 0;
        } else {
            $this->comment_avg = $this->median($this->arrComment);
        }
        return $this->comment_avg;
    }

    /**
     * @param $arr
     * @return int
     */
    private function median($arr) { //Медиана от массива $arr
        sort ($arr);
        $count = count($arr);
        $middle = floor($count / 2);
        if ($count % 2)
            return $arr[$middle];
        else
            return round(($arr[$middle-1] + $arr[$middle]) / 2);
    }

    private function ratingCase1(){
        if ($this->followers_count >= 0 && $this->followers_count < 200) {
            $eval = 0;
        } elseif ($this->followers_count > 200 && $this->followers_count < 500) {
            $eval = 100000;
        } elseif ($this->followers_count > 500 && $this->followers_count < 1000) {
            $eval = 1000000;
        } elseif ($this->followers_count > 1000 && $this->followers_count < 5000) {
            $eval = 2000000;
        } elseif ($this->followers_count > 5000 && $this->followers_count < 10000) {
            $eval = 5000000;
        } elseif ($this->followers_count > 10000 && $this->followers_count < 15000) {
            $eval = 7000000;
        } elseif ($this->followers_count > 15000 && $this->followers_count < 50000) {
            $eval = 10000000;
        } elseif ($this->followers_count > 50000 && $this->followers_count < 100000) {
            $eval = 15000000;
        } elseif ($this->followers_count > 100000 && $this->followers_count < 500000) {
            $eval = 20000000;
        } elseif ($this->followers_count > 500000 && $this->followers_count < 1000000) {
            $eval = 30000000;
        } elseif ($this->followers_count > 1000000 && $this->followers_count < 1500000) {
            $eval = 70000000;
        } elseif ($this->followers_count > 1500000 && $this->followers_count < 2000000) {
            $eval = 95000000;
        } elseif ($this->followers_count > 2000000 && $this->followers_count < 2500000) {
            $eval = 120000000;
        } elseif ($this->followers_count > 2500000 && $this->followers_count < 3000000) {
            $eval = 145000000;
        } elseif ($this->followers_count > 3000000 && $this->followers_count < 3500000) {
            $eval = 170000000;
        } elseif ($this->followers_count > 3500000 && $this->followers_count < 4000000) {
            $eval = 195000000;
        } elseif ($this->followers_count > 4000000 && $this->followers_count < 4500000) {
            $eval = 220000000;
        } elseif ($this->followers_count > 4500000 && $this->followers_count < 5000000) {
            $eval = 245000000;
        } elseif ($this->followers_count > 5000000 && $this->followers_count < 5500000) {
            $eval = 270000000;
        } elseif ($this->followers_count > 5500000 && $this->followers_count < 6000000) {
            $eval = 295000000;
        } elseif ($this->followers_count > 6000000 && $this->followers_count < 6500000) {
            $eval = 320000000;
        } elseif ($this->followers_count > 6500000 && $this->followers_count < 7000000) {
            $eval = 345000000;
        } elseif ($this->followers_count > 7000000 && $this->followers_count < 7500000) {
            $eval = 370000000;
        } elseif ($this->followers_count > 7500000 && $this->followers_count < 8000000) {
            $eval = 395000000;
        } elseif ($this->followers_count > 8000000 && $this->followers_count < 8500000) {
            $eval = 420000000;
        } elseif ($this->followers_count > 8500000 && $this->followers_count < 9000000) {
            $eval = 445000000;
        } elseif ($this->followers_count > 9000000 && $this->followers_count < 9500000) {
            $eval = 470000000;
        } elseif ($this->followers_count > 9500000 && $this->followers_count < 10000000) {
            $eval = 495000000;
        } elseif ($this->followers_count > 10000000 && $this->followers_count < 10500000) {
            $eval = 520000000;
        } elseif ($this->followers_count > 10500000 && $this->followers_count < 11000000) {
            $eval = 545000000;
        } elseif ($this->followers_count > 11000000 && $this->followers_count < 11500000) {
            $eval = 570000000;
        } elseif ($this->followers_count > 11500000 && $this->followers_count < 12000000) {
            $eval = 595000000;
        } elseif ($this->followers_count > 12000000 && $this->followers_count < 12500000) {
            $eval = 620000000;
        } elseif ($this->followers_count > 12500000 && $this->followers_count < 13000000) {
            $eval = 645000000;
        } elseif ($this->followers_count > 13000000 && $this->followers_count < 13500000) {
            $eval = 670000000;
        } elseif ($this->followers_count > 13500000 && $this->followers_count < 14000000) {
            $eval = 695000000;
        } elseif ($this->followers_count > 14000000 && $this->followers_count < 14500000) {
            $eval = 720000000;
        } elseif ($this->followers_count > 14500000 && $this->followers_count < 15000000) {
            $eval = 745000000;
        } elseif ($this->followers_count > 15000000 && $this->followers_count < 15500000) {
            $eval = 770000000;
        } elseif ($this->followers_count > 15500000 && $this->followers_count < 16000000) {
            $eval = 795000000;
        } elseif ($this->followers_count > 16000000 && $this->followers_count < 16500000) {
            $eval = 820000000;
        } elseif ($this->followers_count > 16500000 && $this->followers_count < 17000000) {
            $eval = 845000000;
        } elseif ($this->followers_count > 17000000 && $this->followers_count < 17500000) {
            $eval = 870000000;
        } elseif ($this->followers_count > 17500000 && $this->followers_count < 18000000) {
            $eval = 895000000;
        } elseif ($this->followers_count > 18000000 && $this->followers_count < 18500000) {
            $eval = 920000000;
        } elseif ($this->followers_count > 18500000 && $this->followers_count < 19000000) {
            $eval = 945000000;
        } elseif ($this->followers_count > 19000000 && $this->followers_count < 19500000) {
            $eval = 970000000;
        } elseif ($this->followers_count > 19500000 && $this->followers_count < 20000000) {
            $eval = 995000000;
        } elseif ($this->followers_count > 20000000 && $this->followers_count < 20500000) {
            $eval = 1020000000;
        } elseif ($this->followers_count > 20500000 && $this->followers_count < 21000000) {
            $eval = 1045000000;
        } elseif ($this->followers_count > 21000000 && $this->followers_count < 21500000) {
            $eval = 1070000000;
        } elseif ($this->followers_count > 21500000 && $this->followers_count < 22000000) {
            $eval = 1095000000;
        } elseif ($this->followers_count > 22000000 && $this->followers_count < 22500000) {
            $eval = 1120000000;
        } elseif ($this->followers_count > 22500000 && $this->followers_count < 23000000) {
            $eval = 1145000000;
        } elseif ($this->followers_count > 23000000 && $this->followers_count < 23500000) {
            $eval = 1170000000;
        } elseif ($this->followers_count > 23500000 && $this->followers_count < 24000000) {
            $eval = 1195000000;
        } elseif ($this->followers_count > 24000000 && $this->followers_count < 24500000) {
            $eval = 1220000000;
        } elseif ($this->followers_count > 24500000 && $this->followers_count < 25000000) {
            $eval = 1245000000;
        } elseif ($this->followers_count > 25000000 && $this->followers_count < 25500000) {
            $eval = 1270000000;
        } elseif ($this->followers_count > 25500000 && $this->followers_count < 26000000) {
            $eval = 1295000000;
        } elseif ($this->followers_count > 26000000 && $this->followers_count < 26500000) {
            $eval = 1320000000;
        } elseif ($this->followers_count > 26500000 && $this->followers_count < 27000000) {
            $eval = 1345000000;
        } elseif ($this->followers_count > 27000000 && $this->followers_count < 27500000) {
            $eval = 1370000000;
        } elseif ($this->followers_count > 27500000 && $this->followers_count < 28000000) {
            $eval = 1395000000;
        } elseif ($this->followers_count > 28000000 && $this->followers_count < 28500000) {
            $eval = 1420000000;
        } elseif ($this->followers_count > 28500000 && $this->followers_count < 29000000) {
            $eval = 1445000000;
        } elseif ($this->followers_count > 29000000 && $this->followers_count < 29500000) {
            $eval = 1470000000;
        } elseif ($this->followers_count > 29500000 && $this->followers_count < 30000000) {
            $eval = 1495000000;
        } elseif ($this->followers_count > 30000000 && $this->followers_count < 30500000) {
            $eval = 1520000000;
        } elseif ($this->followers_count > 30500000 && $this->followers_count < 31000000) {
            $eval = 1545000000;
        } elseif ($this->followers_count > 31000000 && $this->followers_count < 31500000) {
            $eval = 1570000000;
        } elseif ($this->followers_count > 31500000 && $this->followers_count < 32000000) {
            $eval = 1595000000;
        } elseif ($this->followers_count > 32000000 && $this->followers_count < 32500000) {
            $eval = 1620000000;
        } elseif ($this->followers_count > 32500000 && $this->followers_count < 33000000) {
            $eval = 1645000000;
        } elseif ($this->followers_count > 33000000 && $this->followers_count < 33500000) {
            $eval = 1670000000;
        } elseif ($this->followers_count > 33500000 && $this->followers_count < 34000000) {
            $eval = 1695000000;
        } elseif ($this->followers_count > 34000000 && $this->followers_count < 34500000) {
            $eval = 1520000000;
        } elseif ($this->followers_count > 34500000 && $this->followers_count < 35000000) {
            $eval = 1545000000;
        } elseif ($this->followers_count > 35000000 && $this->followers_count < 35500000) {
            $eval = 1570000000;
        } elseif ($this->followers_count > 35500000 && $this->followers_count < 36000000) {
            $eval = 1595000000;
        } elseif ($this->followers_count > 36000000 && $this->followers_count < 36500000) {
            $eval = 1620000000;
        } elseif ($this->followers_count > 36500000 && $this->followers_count < 37000000) {
            $eval = 1645000000;
        } elseif ($this->followers_count > 37000000 && $this->followers_count < 37500000) {
            $eval = 1670000000;
        } elseif ($this->followers_count > 37500000 && $this->followers_count < 38000000) {
            $eval = 1695000000;
        } elseif ($this->followers_count > 38000000 && $this->followers_count < 38500000) {
            $eval = 1720000000;
        } elseif ($this->followers_count > 38500000 && $this->followers_count < 39000000) {
            $eval = 1745000000;
        } elseif ($this->followers_count > 39000000 && $this->followers_count < 39500000) {
            $eval = 1770000000;
        } elseif ($this->followers_count > 39500000 && $this->followers_count < 40000000) {
            $eval = 1795000000;
        }else {
            $eval = 3000000000;
        }
        if ($this->following_count >= 0 && $this->following_count < 500) {
            $eval += 10000000;
        } elseif ($this->following_count > 500 && $this->following_count < 1000) {
            $eval += 7000000;
        } elseif ($this->following_count > 1000 && $this->following_count < 1500) {
            $eval += 5000000;
        } elseif ($this->following_count > 1500 && $this->following_count < 2000) {
            $eval += 0;
        } elseif ($this->following_count > 2000 && $this->following_count < 3000) {
            $eval -= 100000000;
        } elseif ($this->following_count > 3000 && $this->following_count < 7500) {
            $eval -= 5000000000;
        } else {
            $eval -= 10000000000;
        }
        if ($this->media_count >= 0 && $this->media_count < 10) {
            $eval += 0;
        } elseif ($this->media_count > 10 && $this->media_count < 50) {
            $eval += 100000;
        } elseif ($this->media_count > 50 && $this->media_count < 100) {
            $eval += 500000;
        } elseif ($this->media_count > 100 && $this->media_count < 500) {
            $eval += 1000000;
        } elseif ($this->media_count > 500 && $this->media_count < 1000) {
            $eval += 5000000;
        } else {
            $eval += 50000000;
        }

        if ($eval < 0) {
            $eval = 1;
        }

        if ($this->max_followers_count == 0) $this->max_followers_count = 226282727;
        if ($this->max_er == 0) $this->max_er = 0.32;

        $M = $this->followers_count / $this->max_followers_count;
        $err = $this->er/100;
        $H = ($this->followers_count * $err) / ($this->max_followers_count * ($this->max_er/100));
        $this->rating = round(sqrt(($M + $H) * $eval));
    }

    private function ratingCase2(){
        $sum = $this->comment_avg + $this->like_avg;
        if ($sum <= 0) $sum = 1;
        // Коэффициенты нужно подобрать эмпирически
        // они влеяют на скорость возрастания рейтинга, зависисость линейкая
        $A = 0.5;
        $B = 1;
        $C = 2;
        // Данное условие даёт штраф если колличество лайков и комментов превысят колличество подписчиов в два раза
        if ($this->followers_count < 2 * $sum){
            $B = 0.1;
        }
        // в первом случае при увеличении колличество подписок до 2000 рейтинг увеличивается
        if ($this->following_count >= 0 && $this->following_count < 2000) {
            $sum2 = ($A * $this->followers_count + $B * $sum + $C * $this->media_count + $this->following_count);
        } else {
            // при подписок более 2000 к рейтинг минусуется в зависимости от кол-во подписок, зависимость линейкая
            $sum2 = ($A * $this->followers_count + $B * $sum + $C * $this->media_count - $this->following_count + 4000);
        }


        if ($sum2 <= 0) $sum2 = 1;
        $this->rating = round(sqrt($sum2));
    }

    /**
     * @param int $max_followers_count
     */
    public function setMaxFollowersCount(int $max_followers_count)
    {
        $this->max_followers_count = $max_followers_count;
    }

    /**
     * @param float $max_er
     */
    public function setMaxEr(float $max_er)
    {
        $this->max_er = $max_er;
    }

}