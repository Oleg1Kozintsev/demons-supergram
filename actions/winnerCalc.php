<?php

namespace demonsThebloggers\Actions;

use Medoo\Medoo;
use kozintsev\ALogger\Logger;

class winnerCalc
{
    /**
     * @var Medoo
     */
    private $database;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * winnerCalc constructor.
     * @param Medoo $database
     * @param \Psr\Log\LoggerInterface|null $logger
     */
    public function __construct(Medoo $database, \Psr\Log\LoggerInterface $logger = null)
    {
        $this->database = $database;
        if ($logger === null) {
            $logger = new \Psr\Log\NullLogger();
        }
        $this->logger = $logger;
    }

    /**
     * @param int $postId
     * @param int $validUntil
     * @param int $type 0 - default
     * @return int
     */
    public function GetWinnerId($postId, $validUntil, $type): int
    {
        $authorId = $this->database->get('post', 'account_id', ['id' => $postId]);

        $row = [
            'post_id' => $postId,
            'created_at[<]' => $validUntil,
            'account_id[!]' => $authorId
        ];

        $this->logger->info("GetWinnerId start code: {$postId} validUntil: {$validUntil} type: {$type}");

        $count = $this->database->count('comments', $row);

        if ($count === 0) {
            $this->logger->error("GetWinnerId count = 0");
            return 0;
        }

        if ($count < 0) {
            $this->logger->error("GetWinnerId count < 0");
            return 0;
        }

        $arr = $this->database->select('comments', 'id', $row);

        if ($count === 1) {
            $this->logger->warning("One participant. GetWinnerId commentId = " . $arr[0]);
            return $this->SaveWinner($arr[0], $postId);
        }

        switch ($type) {
            case 0:
                return $this->SimpleCompetition($arr, $count, $postId);
            case 1:
                return $this->IndicateNFiends($row, $postId, 2);
            case 2:
                return $this->IndicateNFiends($row, $postId, 3);
            default:
                return 0;
        }
    }

    /**
     * @param array $arr
     * @param int $count
     * @param int $postId
     * @return int
     */
    private function SimpleCompetition($arr, $count, $postId): int
    {
        $i = 0;
        $commentId = 0;

        try {
            $r = random_int(0, $count - 1);
            foreach ($arr as $item) {
                if ($r === $i) {
                    $commentId = $item;
                    break;
                }
                $i++;
            }
        } catch (\Exception $e) {
            $this->logger->error("GetWinnerId random Exception " . $e->getMessage());
            return 0;
        }

        if ($commentId === 0) {
            $this->logger->warning("GetWinnerId commentId = 0");
            return 0;
        }

        return $this->SaveWinner($commentId, $postId);
    }

    /**
     * @param $row
     * @param $postId
     * @param $n
     * @return int
     */
    private function IndicateNFiends($row, $postId, $n)
    {

        $arr = $this->database->select('comments', ['id', 'caption'], $row);
        $arr_in = [];
        foreach ($arr as $item) {
            $str = $item['caption'];
            $res = explode('@', $str);
            if (count($res) > $n) {
                $arr_in[] = $item['id'];
            }
        }

        $count = count($arr_in);

        if ($count === 0) {
            $this->logger->warning("GetWinnerId count = 0");
            return 0;
        }

        if ($count < 0) {
            $this->logger->error("GetWinnerId count < 0");
            return 0;
        }

        if($count === 1){
            return $this->SaveWinner($arr_in[0], $postId);
        }

        $i = 0;
        $commentId = 0;

        try {
            $rnd = random_int(0, $count - 1);
            foreach ($arr_in as $item) {
                if ($rnd === $i) {
                    $commentId = $item;
                    break;
                }
                $i++;
            }

        } catch (\Exception $e) {
            $this->logger->error("GetWinnerId random Exception " . $e->getMessage());
            return 0;
        }

        if ($commentId === 0) {
            $this->logger->warning("GetWinnerId commentId = 0");
            return 0;
        }

        return $this->SaveWinner($commentId, $postId);
    }

    /**
     * @param int $id comment id
     * @param int $postId
     * @return int
     */
    private function SaveWinner($id, $postId): int
    {
        $data = $this->database->get('comments', [
            'account_id'
        ], [
            'id' => $id
        ]);
        if (empty($data)) {
            $accountId = 0;
        } else {
            $accountId = $data['account_id'];
        }
        if ($accountId === 0)
            return 0;
        $this->database->insert('winner', [
            "post_id" => $postId,
            "account_id" => $accountId
        ]);
        return $accountId;
    }
}