<?php
namespace demonsThebloggers\Actions;

use Medoo\Medoo;
use kozintsev\ALogger\Logger;

class updateCache
{
    /**
     * @param Medoo $database
     * @param $username
     * @param Logger $logger
     */
    public static function updateCacheStatsOneAccount(Medoo $database, $username, Logger $logger){
        $where = [
            "username" => $username
        ];
        self::updateCacheStatsAccounts($database, $logger, $where);
    }

    /**
     * @param Medoo $database
     * @param Logger $logger
     * @param array $where
     */
    public static function updateCacheStatsAccounts(Medoo $database, Logger $logger, $where = []){
        $where = array_merge($where, [
            "is_sync" => 1,
            "is_error" => 0,
            "is_do" => 1,
            "is_404" => 0
        ]);

        $rows = $database->select("stats_accounts", [
            "id", "username", "pk", "followers_count", "followings_count", "media_count",
            "like_avg", "comment_avg", "rating", "er", "lr", "tr", "created_at", "updated_at"
        ], $where);

        echo "Select stats accounts \n";


        foreach ($rows as $item) {
            $count = $database->count("cache_stats_accounts",  [
                "username" => $item['username']
            ]);
            // числа это процент не фоловеров, чужих аккаунтов
            if ($item['id'] == 1153){
                $er_followers = round($item['er'] - $item['er'] * 0.21, 2);
                $lr_followers = round($item['lr'] - $item['lr'] * 0.21, 2);
                $tr_followers = round($item['tr'] - $item['tr'] * 0.09, 2);
            } else {
                $er_followers = $item['er'];
                $lr_followers = $item['lr'];
                $tr_followers = $item['tr'];
            }
            if ($count == 1){
                $data = $database->update("cache_stats_accounts", [
                    "username" => $item['username'],
                    "pk" => $item['pk'],
                    "followers_count" => $item['followers_count'],
                    "followings_count" => $item['followings_count'],
                    "media_count" => $item['media_count'],
                    "like_avg" => $item['like_avg'],
                    "comment_avg" => $item['comment_avg'],
                    "rating" => $item['rating'],
                    "er" => $item['er'],
                    "er_followers" => $er_followers,
                    "lr" => $item['lr'],
                    "lr_followers" =>  $lr_followers,
                    "tr" => $item['tr'],
                    "tr_followers" => $tr_followers,
                    "created_at" => $item['created_at'],
                    "updated_at" => $item['updated_at'],
                    "is_sync" => 1
                ], [
                    "id" => $item['id']
                ]);
            } else {
                $data = $database->insert("cache_stats_accounts", [
                    "id" => $item['id'],
                    "username" => $item['username'],
                    "pk" => $item['pk'],
                    "followers_count" => $item['followers_count'],
                    "followings_count" => $item['followings_count'],
                    "media_count" => $item['media_count'],
                    "like_avg" => $item['like_avg'],
                    "comment_avg" => $item['comment_avg'],
                    "rating" => $item['rating'],
                    "er" => $item['er'],
                    "er_followers" => $er_followers,
                    "lr" => $item['lr'],
                    "lr_followers" =>  $lr_followers,
                    "tr" => $item['tr'],
                    "tr_followers" => $tr_followers,
                    "created_at" => $item['created_at'],
                    "updated_at" => $item['updated_at'],
                    "is_sync" => 1
                ]);
            }

            if ($data->rowCount() === 0) {
                $logger->debug("Data not update. Username: {$item['username']} Id: {$item['id']}");
            } else {
                $logger->debug("Update success. Username: {$item['username']} Id: {$item['id']}");
            }
        }
        echo "Update cache stats accounts \n";
    }

    /**
     * @param Medoo $database
     * @param int $id
     */
    public static function updateCacheTopFollowers(Medoo $database, $id){

        $rows = $database->delete("cache_top_followers", [
            "user_id" => $id
        ]);

        echo "Delete cache top followers {$rows->rowCount()} \n";

        $sql = <<<SQL
INSERT INTO cache_top_followers(user_id, follower_id, username, followers_count, fullname, profilePicUrl)
SELECT stats_followers.user_id, stats_accounts.id, stats_accounts.username,  stats_accounts.followers_count, stats_accounts.fullname, stats_accounts.profilePicUrl
FROM stats_followers INNER JOIN stats_accounts ON stats_followers.follower_id = stats_accounts.id AND stats_followers.user_id = :user_id
ORDER BY stats_accounts.followers_count DESC LIMIT 0, 30;
SQL;

        $data = $database->query(
            $sql, [
                ":user_id" => $id
            ]
        )->fetchAll();

        echo "Insert cache top followers \n";

        print_r($data);
    }
}