<?php

namespace demonsThebloggers\Actions;

use DateTime;
use Exception;

use InstagramAPI\Instagram;
use InstagramAPI\Response\Model\User;
use InstagramAPI\Response\UserInfoResponse;
use Medoo\Medoo;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use demonsThebloggers\Models\Post;

class accountAnalyzer
{

    /** username by instagram
     * @var string
     */
    private $username;
    /**
     * @var Medoo
     */
    private $database;
    /**
     * @var Instagram
     */
    private $ig;
    /**
     * id пользователя в базе статистики, если 0 то аккаунта нет в базе данных статистики, получается в конструкторе
     * @var int
     */
    private $id;
    /**
     * ID by Instagram для получения ленты пользователя instagram pk
     * @var
     */
    private $pk;

    /**
     * счётчик для колличества добавленных фоловеров
     * @var int
     */
    private $user_added;


    /**
     * @var string
     */
    private $maxId;

    /**
     * @return string
     */
    public function getMaxId()
    {
        return $this->maxId;
    }

    /**
     * Поиск pk instagram по имени пользователя
     * @internal param mixed $userId
     */
    public function getPk()
    {
        if (empty($this->pk) || $this->pk === 0) {
            if (!empty($this->username)) {
                try {
                    $this->pk = $this->ig->people->getUserIdForName($this->username);
                } catch (\Exception $e) {
                    echo 'getUserIdByInst : Something went wrong: ' . $e->getMessage() . "\n";
                    $this->error = true;
                    return 0;
                }
            } else {
                throw new Exception("Username not found");
            }
        }
        return $this->pk;
    }

    /**
     * @var bool
     */
    private $error = false;
    /**
     * @var string
     */
    private $message = '';
    /**
     * @var int
     */
    private $req_auth = 0;

    /**
     * @var Logger
     */
    private $logger;

    const OneDay = 86400; // 1 day = 86400 sec

    /**
     * account is array, for example:
     * [
     *  'username' => 'Joy22',
     *  'pk' => 777777
     * ]
     * accountAnalyzer constructor.
     * @param Medoo $database
     * @param Instagram $ig
     * @param array|mixed[] $account - username or pk (string or int)
     * @param string $log_file
     * @throws Exception
     */
    public function __construct(Medoo $database, Instagram $ig, array $account,
                                $log_file = "")
    {
        $this->database = $database;
        $this->ig = $ig;
        $this->maxId = null;

        if (isset($account['username'])) {
            $this->username = $account['username'];
            $this->id = $this->findUserId($this->username);
        }
        if (isset($account['pk'])) {
            $this->pk = $account['pk'];
            $this->id = $this->findUserIdByPk($this->pk);
        } else {
            $this->pk = $this->getPk();
        }
        if (empty($account['username']) && empty($account['pk'])) {
            throw new Exception('accountAnalyzer error. Username and pk not found!');
        }

        $this->logger = new Logger('account-analyzer');
        $this->logger->pushHandler(new StreamHandler($log_file, Logger::INFO));
    }

    /**
     * @param $username
     * @return int
     */
    public function findUserId($username): int
    {
        $data = $this->database->get('stats_accounts', [
            'id'
        ], [
            'username' => $username
        ]);
        if (empty($data)) {
            return 0;
        } else {
            return $data['id'];
        }
    }

    /**
     * @param $pk
     * @return int
     */
    public function findUserIdByPk($pk): int
    {
        $data = $this->database->get('stats_accounts', [
            'id'
        ], [
            'pk' => $pk
        ]);
        if (empty($data)) {
            return 0;
        } else {
            return $data['id'];
        }
    }

    /**
     * Подписчики
     * @param callable $_getOutOfLoop

     * @param null|integer $maxId
     * @param bool $is_drop
     * @return bool
     * @internal param callable $getOutOfLoop
     */
    public function getFollowers(callable $_getOutOfLoop, $maxId = null, $is_drop = false)
    {
        echo "GetFollowers start Memory used: " . memory_get_usage() . "\n";

        $this->logger->info("GetFollowers start Memory used: " . memory_get_usage());

        if ($is_drop && $this->id !== 0) {
            $this->database->delete('stats_followers', [
                'user_id' => $this->id,
            ]);
            $this->logger->info("Drop followers");
        }
        $i = 0;

        try {
            do {
                $response = $this->ig->people->getFollowers($this->pk, null, $maxId);
                $maxId = $response->getNextMaxId();
                $followers = $response->getUsers();
                $this->user_added = 0;
                foreach ($followers as $follower) {
                    if ($follower instanceof User) {
                        $f_id = $this->getAccountByStatsLight($follower, 0);
                        if ($f_id == 0) {
                            $this->logger->error("Iteration: " . $i . " f_id == 0");
                            continue;
                        }
                        $date = new DateTime();
                        $timestamp = $date->getTimestamp();
                        if ($is_drop === false) {
                            $count = $this->database->count("stats_followers", [
                                'user_id' => $this->id,
                                'follower_id' => $f_id
                            ]);
                            if ($count === 0) {
                                $this->database->insert('stats_followers', [
                                    'user_id' => $this->id,
                                    'follower_id' => $f_id,
                                    'created_at' => $timestamp
                                ]);
                            }
                        } else {
                            $this->database->insert('stats_followers', [
                                'user_id' => $this->id,
                                'follower_id' => $f_id,
                                'created_at' => $timestamp
                            ]);
                        }
                    }
                }
                $i++;
                $this->logger->info("Iteration: " . $i . " Memory used: " . memory_get_usage() . " maxId: " . $maxId . " Number of users added: " . $this->user_added);
                $this->maxId = $maxId;
                if ($_getOutOfLoop($this, $this->logger) === true) break;
                sleep(3);
            } while ($maxId !== null);
        } catch (\Exception $e) {
            $this->message = $e->getMessage();
            echo 'Something went wrong: ' . $this->message . "\n";
            $this->logger->error("Iteration: " . $i . " maxId: " . $maxId);
            $this->maxId = $maxId;
            return false;
        }
        return true;
    }


    /** Поиск аккаунта в интсаграмме, должно быть точное совпадение
     * @return bool
     */
    public function SearchAccount()
    {
        try {
            $info = $this->ig->people->search($this->username);
        } catch (\Exception $e) {
            return false;
        }
        if (count($info->getUsers()) == 0) {
            return false;
        } else {
            foreach ($info->getUsers() as $user) {
                if ($user->getUsername() === $this->username)
                    return true;
            }
        }
        return false;
    }

    public function isError(): bool
    {
        return $this->error;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return array|null
     * [
     * 'fullname' => '',
     * 'profilePicUrl' => '',
     * ]
     */
    public function SearchInStatistic()
    {
        $rows = $this->database->select("stats_accounts", [
            "fullname", "profilePicUrl"
        ], [
            "username" => $this->username,
            'is_sync' => 1,
            "is_error" => 0
        ]);
        foreach ($rows as $item) {
            return [
                'fullname' => $item['fullname'],
                'profilePicUrl' => $item['profilePicUrl'],
            ];
        }
        return null;
    }

    /**
     * Получить инфомацию о пользователе и добавить данные в базу данных статистики
     * @param bool $is_like_stat - статистика по последнему посту
     * @param bool $is_data_check проверять дату, для первых суток
     * @param bool $get_top получать ТОП постов или нет
     * @return bool
     * @throws Exception
     */
    public function GetAccountInfo($is_like_stat = False, $is_data_check = True, $get_top = True): bool
    {
        try {
            if ($this->pk === 0) {
                $info = $this->ig->people->getInfoByName($this->username);
                $this->pk = $info->getUser()->getPk();
            } else {
                $info = $this->ig->people->getInfoById($this->pk);
            }
        } catch (\Exception $e) {
            $this->message = $e->getMessage();
            $this->setErrorAttr();
            return false;
        }
        if (!$is_like_stat) {
            $this->id = $this->collectionOfAccountStatistics($info, $get_top);
        } else {
            $this->collectionOfLikeStatistics($is_data_check);
        }
        return true;
    }

    /**
     * @param bool $is_only_last
     * @param bool $need_top
     * @return bool
     */
    public function GetPostsInfo($is_only_last = False, $need_top = False)
    {
        try {
            $info = $this->ig->people->getInfoByName($this->username);
            $this->pk = $this->ig->people->getUserIdForName($this->username);
            $media_count = $info->getUser()->getMediaCount();
        } catch (\Exception $e) {
            $this->message = $e->getMessage();
            $this->setErrorAttr();
            return false;
        }
        $max_count = round($media_count / 18);
        if ($max_count > 10) $max_count = 10;
        $this->getPostsTop($is_only_last, $need_top, $max_count);
        return true;
    }

    /**
     * @param string $mediaId
     * @return bool
     */
    public function collectionOfLikers($mediaId)
    {
        $this->logger->info('collectionOfLikers start');
        try {
            $likers = $this->ig->media->getLikers((string)$mediaId);
        } catch (\Exception $e) {
            $this->message = $e->getMessage();
            return false;
        }
        if (count($likers->getUsers()) === 0) {
            $this->logger->info("collectionOfLikers likers count 0");
            return true;
        }
        $this->logger->debug('Start loop getUser');

        foreach ($likers->getUsers() as $user) {
            $username = $user->getUsername();
            $pk = $user->getPk();
            $this->logger->debug("Collection of likers username: " . $username . " pk:" . $pk);
            $targetId = $this->findUserIdByPk($pk);
            if ($targetId === 0) {
                $targetId = $this->findUserId($username);
            }
            if ($targetId === 0) {
                $row = [
                    'username' => $username,
                    'followers_count' => $user->getFollowerCount(),
                    'followings_count' => $user->getFollowingCount(),
                    'media_count' => $user->getMediaCount(),
                    'fullname' => $user->getFullName(),
                    'email' => $user->getEmail(),
                    'biography' => $user->getBiography(),
                    'birthday' => $user->getBirthday(),
                    'latitude' => $user->getLatitude(),
                    'longitude' => $user->getLongitude(),
                    'publicphonenumber' => $user->getPublicPhoneNumber(),
                    'socialcontext' => $user->getSocialContext(),
                    'profilePicUrl' => $user->getProfilePicUrl(),
                    'pk' => $pk,
                    'is_sync' => 1,
                    'is_do' => 0
                ];
                $data = $this->database->insert('stats_accounts', $row);
                $this->logger->debug("Insert. Row count: " . $data->rowCount());
                $targetId = $this->database->id();
                $this->logger->debug("Insert new user id: " . $targetId);

            }
            if ($targetId === 0) {
                echo "GetUserInfo error: " . $this->message;
                $this->logger->error("GetUserInfo error: " . $this->message . ". Username: " . $username);
                continue;
            }
            $date = new DateTime();
            $timestamp = $date->getTimestamp();
            $count = $this->database->count('statistics_likers', [
                'user_id' => $targetId,
                'media_id' => $mediaId
            ]);
            if ($count == 0) {
                $this->database->insert('statistics_likers',
                    [
                        'user_id' => $targetId,
                        'media_id' => $mediaId,
                        'created_at' => $timestamp
                    ]);
                $this->logger->debug("Add likers. User id:" . $targetId);
            }
        }
        return true;
    }

    /**
     * @param string $mediaId
     * @return bool
     */
    public function collectionOfComments($mediaId)
    {
        // pagination
        $this->logger->info('collectionOfComments start');

        $maxId = null;
        do {
            try {
                $comments = $this->ig->media->getComments((string)$mediaId, $maxId);
                $maxId = $comments->getNextMaxId();
            } catch (\Exception $e) {
                $this->message = $e->getMessage();
                return false;
            }
            if (count($comments->getComments()) === 0) {
                $this->logger->debug("collectionOfComments comment count 0");
                return true;
            }
            $this->logger->debug('Start loop getUser');


            foreach ($comments->getComments() as $comment) {
                if (!$comment->isUser())
                    continue;
                $text = '';
                if ($comment->isText()) {
                    $text = $comment->getText();
                }
                $user = $comment->getUser();
                $username = $user->getUsername();
                $pk = $user->getPk();
                $this->logger->debug("Collection of Comments username: " . $username . " pk:" . $pk);
                $targetId = $this->findUserIdByPk($pk);
                if ($targetId === 0) {
                    $targetId = $this->findUserId($username);
                }
                if ($targetId === 0) {
                    $row = [
                        'username' => $username,
                        'followers_count' => $user->getFollowerCount(),
                        'followings_count' => $user->getFollowingCount(),
                        'media_count' => $user->getMediaCount(),
                        'fullname' => $user->getFullName(),
                        'email' => $user->getEmail(),
                        'biography' => $user->getBiography(),
                        'birthday' => $user->getBirthday(),
                        'latitude' => $user->getLatitude(),
                        'longitude' => $user->getLongitude(),
                        'publicphonenumber' => $user->getPublicPhoneNumber(),
                        'socialcontext' => $user->getSocialContext(),
                        'profilePicUrl' => $user->getProfilePicUrl(),
                        'pk' => $pk,
                        'is_sync' => 1,
                        'is_do' => 0
                    ];
                    $data = $this->database->insert('stats_accounts', $row);
                    $this->logger->debug("Insert. Row count: " . $data->rowCount());
                    $targetId = $this->database->id();
                    $this->logger->debug("Insert new user id: " . $targetId);

                }
                if ($targetId === 0) {
                    echo "GetUserInfo error: " . $this->message;
                    $this->logger->error("GetUserInfo error: " . $this->message . ". Username: " . $username);
                    continue;
                }
                $date = new DateTime();
                $timestamp = $date->getTimestamp();
                $count = $this->database->count('statistics_comments', [
                    'user_id' => $targetId,
                    'media_id' => $mediaId
                ]);
                if ($count == 0) {
                    $this->database->insert('statistics_comments',
                        [
                            'user_id' => $targetId,
                            'media_id' => $mediaId,
                            'created_at' => $timestamp,
                            'text' => $text
                        ]);
                    $this->logger->debug("Add likers. User id:" . $targetId);
                }
            }
            sleep(3);
        } while ($maxId !== null);
        return true;
    }

    public function setAccountError()
    {
        $this->database->update('stats_accounts', [
            'is_error' => 1
        ], [
            'id' => $this->id
        ]);
    }

    public function setAccountDoTrue()
    {
        $this->database->update('stats_accounts', [
            'is_do' => 1
        ], [
            'id' => $this->id
        ]);
    }

    public function setNotAuthAccount()
    {
        $this->database->update('stats_accounts', [
            'req_auth' => 1
        ], [
            'id' => $this->id
        ]);
    }

    public function getId(): int
    {
        return $this->id;
    }

    private function setErrorAttr()
    {
        $this->error = true;
        $pos = strpos($this->message, 'User not found');
        if (!($pos === false)) {
            $this->setAccountError();
            echo "Set account error \n";
        }
        $pos = strpos($this->message, 'Not authorized');
        if (!($pos === false)) {
            $this->req_auth = 1;
            $this->setNotAuthAccount();
            echo "Set not auth account \n";
        }
        $this->message = 'Something went wrong: ' . $this->message . "\n" . "Account: " . $this->username . "\n";
        $this->logger->error($this->message);
        echo $this->message;
    }

    /** Получение и обновление информации о поставх пользоватедя
     * @param $is_data_check
     * @return int
     */
    private function collectionOfLikeStatistics($is_data_check): int
    {
        $id = $this->id;
        if ($id === 0) {
            echo "User not found \n";
            return 0;
        }
        $top = $this->getPostsTop(true, true);
        if (empty($top) && empty($top[0])) {
            echo "Top empty \n";
            return 0;
        }
        $post = $top[0];
        if (!($post instanceof Post)) {
            return 0;
        }

        $date = new DateTime();
        $timestamp = $date->getTimestamp();

        $row = $this->database->select('cache_stats_accounts', [
            'last_media_timestamp', 'last_media_pk'
        ], [
            'id' => $id
        ]);
        if (empty($row)) {
            $this->logger->error("Table cache_stats_accounts is empty");
            return 0;
        }
        $last_media_pk = $row[0]['last_media_pk'];
        if ($post->pk != $last_media_pk) {
            $this->database->delete('stats_likes_last_post', [
                'instagram_id' => $id
            ]);
        } else {
            $current_media_timestamp = $row[0]['last_media_timestamp'];
            if ($current_media_timestamp != 0 && ($timestamp - self::OneDay) > $current_media_timestamp && $is_data_check) {
                echo "Stop not work. One day finish \n";
                return 0;
            }
        }

        $this->database->insert('stats_likes_last_post', [
            'instagram_id' => $id,
            'like_count' => $post->like_count,
            'comment_count' => $post->comment_count,
            'created_at' => $timestamp,
        ]);

        if ($this->database->id() === 0) {
            $this->logger->error("Data is not inserted in stats_likes_last_post id: {$id} https://www.instagram.com/p/{$post->code} ");
        }

        $data = $this->database->update('cache_stats_accounts', [
            'last_media_pk' => $post->pk,
            'last_media_url' => $post->url,
            'last_media_caption' => $post->caption,
            'last_media_timestamp' => $post->timestamp,
            'last_media_code' => $post->code
        ], [
            'id' => $id
        ]);

        if ($data->rowCount() === 0) {
            $this->logger->error("Data is not updated in cache_stats_accounts id: {$id} https://www.instagram.com/p/{$post->code} ");
        }

        return $id;
    }

    /**
     * Проверка изменилилсь ли пользовательские данные
     * @param int $user_id id пользователя в базе данных
     * @param array $row строка пользователя
     * @return bool
     */
    private function isAccountChanged($user_id, $row): bool
    {
        $fields = [
            'followers_count',
            'followings_count',
            'media_count',
        ];
        $data = $this->database->select('stats_accounts', $fields, [
            'id' => $user_id
        ]);
        if (empty($data))
            return false;
        foreach ($fields as $f) {
            if ($data[0][$f] != $row[$f])
                return true;
        }
        return false;
    }

    /**
     * @param Post $post
     */
    private function collectionOfPostStatistics(Post $post)
    {
        $count = $this->database->count('stat_post', ['pk' => $post->pk]);

        $row = [
            'instagram_id' => $this->id,
            'url' => $post->url,
            'caption' => $post->caption,
            'code' => $post->code,
            'taken_at' => $post->timestamp,
            'pk' => $post->pk,
            'like_count' => $post->like_count,
            'comment_count' => $post->comment_count,
        ];

        if (isset($post->location)) {
            $loc = json_encode($post->location);
            $row['location'] = $loc;
        }

        if ($count === 0) {
            $this->database->insert('stat_post', $row);
            if ($this->database->id() === 0) {
                $this->logger->error("Data is not inserted in stat_post");
            }
        } else {
            $data = $this->database->update('stat_post', $row, ['pk' => $post->pk]);
            if ($data->rowCount() === 0) {
                $this->logger->debug("Data is not updated in stat_post id: {$post->pk} https://www.instagram.com/p/{$post->code}");
            }
        }
    }

    /**
     * @param bool $is_only_last True only last post только последную запись
     * @param bool $need_top только ТОП 20 первая пагинация
     * @param int $max_count
     * @return array [Post]
     */
    public function getPostsTop($is_only_last = False, $need_top = True, $max_count = 1)
    {
        $i = 0;
        $top = [];
        try {
            $maxId = null;
            do {
                // Request the page corresponding to maxId.
                $response = $this->ig->timeline->getUserFeed($this->pk, $maxId);
                // In this example we're simply printing the IDs of this page's items.
                foreach ($response->getItems() as $item) {
                    $obj = new Post();
                    $obj->like_count = $item->getLikeCount();
                    $obj->comment_count = $item->getCommentCount();
                    $obj->code = $item->getCode();
                    $media_type = $item->getMediaType();
                    if ($item->isLocation()) {
                        $location = $item->getLocation();
                        $obj->location = $location;
                    }

                    switch ($media_type) {
                        case 1: // PHOTO
                        case 2: // VIDEO
                            if (!empty($item->getImageVersions2()) && $item->getImageVersions2()->isCandidates())
                                $obj->url = $item->getImageVersions2()->getCandidates()[0]->getUrl();
                            break;
                        case 8: // ALBUM
                            if ($item->isCarouselMedia())
                                if (!empty($item->getCarouselMedia()[0]->getImageVersions2() && $item->getCarouselMedia()[0]->getImageVersions2()->isCandidates()))
                                    $obj->url = $item->getCarouselMedia()[0]->getImageVersions2()->getCandidates()[0]->getUrl();
                            break;
                    }

                    if ($item->isCaption())
                        $obj->caption = $item->getCaption()->getText();

                    $obj->timestamp = $item->getTakenAt();
                    $obj->media_type = $media_type;
                    $obj->pk = $item->getPk();
                    if ($need_top === True) $top[] = $obj;
                    // Save at database
                    $this->collectionOfPostStatistics($obj);
                    printf("[%s] https://instagram.com/p/%s/\n", $item->getId(), $item->getCode());
                    if ($is_only_last) break;
                }
                $i++;
                if ($need_top || $is_only_last || $i >= $max_count) break;

                $maxId = $response->getNextMaxId();
                // Instagram will throttle you temporarily for abusing their API!
                echo "Sleeping for 5s...\n";
                sleep(5);
            } while ($maxId !== null); // Must use "!==" for comparison instead of "!=".

        } catch (\Exception $e) {
            $this->message = $e->getMessage();
            $this->error = true;
            $this->setErrorAttr();
            return [];
        }
        if ($need_top === True) {
            return $top;
        } else {
            return [1];
        }
    }

    /**
     * @param UserInfoResponse $info
     * @param bool $get_top
     * @return int
     * @throws Exception
     */
    private function collectionOfAccountStatistics(UserInfoResponse $info, $get_top = True): int
    {
        $user = $info->getUser();
        $pk = $user->getPk();
        $followers_count = $user->getFollowerCount();
        $following_count = $user->getFollowingCount();
        $media_count = $user->getMediaCount();
        $username = $user->getUsername();

        $id = $this->id;

        $top = null;

        if ($get_top) {
            $top = $this->getPostsTop(false, true);
        }

        $rating = new ratingCalc($top, $followers_count, $following_count, $media_count);
        $like_avg = $rating->getLikeAvg();
        $comment_avg = $rating->getCommentAvg();
        $er = $rating->getEr();
        $lr = $rating->getLr();
        $tr = $rating->getTr();
        $rat = $rating->getRating();

        $date = new DateTime();
        $timestamp = $date->getTimestamp();
        $row = [
            'username' => $username,
            'followers_count' => $followers_count,
            'followings_count' => $following_count,
            'media_count' => $media_count,
            'fullname' => $user->getFullName(),
            'email' => $user->getEmail(),
            'biography' => $user->getBiography(),
            'birthday' => $user->getBirthday(),
            'latitude' => $user->getLatitude(),
            'longitude' => $user->getLongitude(),
            'publicphonenumber' => $user->getPublicPhoneNumber(),
            'socialcontext' => $user->getSocialContext(),
            'profilePicUrl' => $user->getProfilePicUrl(),
            'pk' => $pk,
            'is_sync' => 1,
            'like_avg' => $like_avg,
            'comment_avg' => $comment_avg,
            'er' => $er,
            'lr' => $lr,
            'tr' => $tr,
            'rating' => $rat,
            'req_auth' => $this->req_auth,
            'is_business' => $user->getIsBusiness() ? 1 : 0
        ];
        if ($id === 0) {
            $this->database->insert('stats_accounts', $row);
            $id = $this->database->id();
            if ($id === 0) {
                $this->logger->error("Data is not inserted in stats_accounts id = 0 username: {$username}");
            }
            $data = $this->database->update('stats_accounts', [
                'created_at' => $timestamp,
            ], [
                'id' => $id
            ]);
            if ($data->rowCount() === 0) {
                $this->logger->error("Data is Not updated in stats_accounts 3 id: {$id} username: {$username}");
            }
            return $id;
        } else {
            // прежде чем делать добавление в статистику мы должны проверить есть ли изменения
            // а так же пересчитать кол-во лайков и комментов
            if ($this->isAccountChanged($id, $row)) {
                $this->database->insert('stats_timeline', [
                    'instagram_id' => $id,
                    'followers_count' => $followers_count,
                    'followings_count' => $following_count,
                    'media_count' => $media_count,
                    'created_at' => $timestamp,
                    'like_avg' => $like_avg,
                    'comment_avg' => $comment_avg,
                    'er' => $er,
                    'lr' => $lr,
                    'tr' => $tr,
                    'rating' => $rat
                ]);
                if ($this->database->id() === 0) {
                    $this->logger->error("Data is not insert in stats_timeline");
                }
            }
            // то делам обновлени если нет
            $data = $this->database->update('stats_accounts', $row, [
                'id' => $id
            ]);
            if ($data->rowCount() === 0) {
                $this->logger->debug("Data is not updated in stats_accounts 1 id: {$id} username: {$username}");
            }
            $data = $this->database->update('stats_accounts', [
                'updated_at' => $timestamp,
            ], [
                'id' => $id
            ]);
            if ($data->rowCount() === 0) {
                $this->logger->debug("Data is not updated in stats_accounts 2 id: {$id} username: {$username}");
            }
            return $id;
        }
    }

    /** СУПЕР Лёгкий вариант для добавления данных в статистику, только добавление и получения ID, если данные есть
     * запрос с помощью getInfoById не делаем
     * @param User $user
     * @param int $is_do
     * @return int
     */
    private function getAccountByStatsLight(User $user, $is_do = 0): int
    {
        $username = $user->getUsername();
        $pk = $user->getPk();
        $date = new DateTime();
        $timestamp = $date->getTimestamp();
        $id = $this->findUserIdByPk($pk);
        if ($id === 0) {
            $id = $this->findUserId($username);
        }
        if ($id === 0) {
            $row = [
                'username' => $username,
                'followers_count' => $user->getFollowerCount(),
                'followings_count' => $user->getFollowingCount(),
                'media_count' => $user->getMediaCount(),
                'fullname' => $user->getFullName(),
                'email' => $user->getEmail(),
                'biography' => $user->getBiography(),
                'birthday' => $user->getBirthday(),
                'latitude' => $user->getLatitude(),
                'longitude' => $user->getLongitude(),
                'publicphonenumber' => $user->getPublicPhoneNumber(),
                'socialcontext' => $user->getSocialContext(),
                'profilePicUrl' => $user->getProfilePicUrl(),
                'created_at' => $timestamp,
                'pk' => $pk,
                'is_sync' => 1,
                'is_do' => $is_do
            ];
            $this->database->insert('stats_accounts', $row);
            $id = $this->database->id();
            $this->logger->debug("Add user: " . $username . " Id: " . $id);
            $this->user_added++;
            return $id;
        } else {
            return $id;
        }
    }
}