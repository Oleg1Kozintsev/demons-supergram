<?php

namespace demonsThebloggers\Actions;

use InstagramAPI\Instagram;
use Medoo\Medoo;
use kozintsev\ALogger\Logger;

class postAnalyzer
{
    /**
     * @var Medoo
     */
    private $database;
    /**
     * @var Instagram
     */
    private $ig;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var int
     */
    private $idNewPost;

    /** @var int */
    private $postId;

    /** @var int */
    private $status;

    /** @var string */
    private $mediaId;

    /**
     * @return string
     */
    public function getMediaId(): string
    {
        return $this->mediaId;
    }

    /**
     * @return int
     */
    public function getPostId(): int
    {
        return $this->postId;
    }

    /**
     * @return int
     * 1 - штатная обработка
     * 2 - не могу получить код поста
     * 3 - ошибка при добавлени поста в базу данных
     * 4 - не возможно определить колличество комментариев
     * 5 - комментариев более чем 1000
     * 6 - комментариев более чем 10000
     * 7 - ошибка при вычислении случайного пользователя в winnerCalc
     * 8 - завершён сбор комментариев для формирование CSV
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * postAnalyzer constructor.
     * @param Medoo $database
     * @param Instagram $ig
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(Medoo $database, Instagram $ig,
                                \Psr\Log\LoggerInterface $logger = null)
    {
        $this->database = $database;
        $this->ig = $ig;
        $this->status = 0;
        if ($logger === null) {
            $logger = new \Psr\Log\NullLogger();
        }
        $this->logger = $logger;
        $this->logger->info('postAnalyzer start');
    }

    /**
     * @param string $code
     * @return bool
     */
    public function getPostInfoFromDb($code): bool
    {
        $id = new \InstagramAPI\InstagramID();
        $this->mediaId = $id->fromCode($code);
        $data = $this->database->select('post', [
            'id'
        ], [
            'pk' => $this->mediaId
        ]);
        foreach ($data as $item)
        {
            $this->postId = $item['id'];
            return true;
        }
        return false;
    }

    /**
     * @param string $code
     * @param $idNewPost
     * @return bool
     */
    public function getPostInfo($code, $idNewPost): bool
    {
        $this->idNewPost = $idNewPost;
        $this->status = 1;

        $id = new \InstagramAPI\InstagramID();
        $this->mediaId = $id->fromCode($code);

        $this->logger->info("getPostInfo start code: {$code} mediaId: {$this->mediaId}");

        $info = $this->ig->media->getInfo((string)$this->mediaId);
        $items = $info->getItems();
        if (empty($items[0])) {
            $this->logger->error("Error getting post info code: {$code} mediaId: {$this->mediaId}");
            $this->status = 2;
            return false;
        }
        sleep(2);
        $user = $items[0]->getUser();
        $userId = $this->FindAndAddAccount($user);
        if ($userId === 0) {
            $this->logger->error("Error adding account");
            $this->status = 2;
            return false;
        }
        $this->postId = $this->FindAndAddPost($this->mediaId, $items[0], $idNewPost, $userId);
        if ($this->postId === 0) {
            $this->logger->error("Error adding post {$idNewPost} {$this->mediaId}");
            $this->status = 3;
            return false;
        }

        $commentCount = $this->GetCommentCount($this->postId);

        if ($commentCount === 0) {
            $this->logger->warning("Not defined Number of Comments {$idNewPost} {$this->mediaId}");
            $this->status = 4;
            return false;
        }

        if ($commentCount > 1000) {
            $this->logger->warning("Too many comments over 1000");
            $this->status = 5;
            $this->logger->debug("Too many comments {$idNewPost} {$this->mediaId}");
            $this->logger->debug("User Id: {$userId} Post Id: {$this->postId}");
        }

        if ($commentCount > 10000) {
            $this->logger->warning("Too many comments over 10000");
            $this->status = 6;
            return false;
        }

        return true;
    }

    public function getComments()
    {
        $maxId = null;

        $this->logger->debug("getComments start mediaID  {$this->mediaId}");

        if (empty($this->mediaId)) {
            $this->logger->error("getComments Media id not found");
            return;
        }

        if (empty($this->postId)) {
            $this->logger->error("getComments Post id not found");
            return;
        }

        do {

            $comments = $this->ig->media->getComments($this->mediaId, [
                'max_id' => $maxId
            ]);

            foreach ($comments->getComments() as $comment) {
                $this->AddComment($comment, $this->postId);
            }

            $maxId = $comments->getNextMaxId();

            //echo "Sleeping for 4s...\n";
            sleep(4);
        } while ($maxId !== null); // Must use "!==" for comparison instead of "!="
    }

    /**
     * @param int $pk
     * @return int
     */
    public function findUserIdByPk($pk): int
    {
        $data = $this->database->get('account', [
            'id'
        ], [
            'pk' => $pk
        ]);
        if (empty($data)) {
            return 0;
        } else {
            return $data['id'];
        }
    }

    /**
     * @param int $pk
     * @return int
     */
    public function findMediaByPk($pk): int
    {
        $data = $this->database->get('post', [
            'id'
        ], [
            'pk' => $pk
        ]);
        if (empty($data)) {
            return 0;
        } else {
            return $data['id'];
        }
    }


    /**
     * @param \InstagramAPI\Response\Model\User $user
     * @return int
     */
    private function FindAndAddAccount($user)
    {
        $targetId = $this->findUserIdByPk($user->getPk());
        if ($targetId === 0) {
            $data = $this->database->insert('account', [
                'pk' => $user->getPk(),
                'username' => $user->getUsername(),
                'fullname' => $user->getFullName(),
                'profilePicUrl' => $user->getProfilePicUrl()
            ]);
            $this->logger->debug("FindAndAddAccount. Insert. Row count: " . $data->rowCount());
            $targetId = $this->database->id();
        }
        return $targetId;
    }

    /**
     * @param \InstagramAPI\Response\Model\Item $item
     * @return null|string
     */
    private function GetMediaUrlFirst($item)
    {
        $media_type = $item->getMediaType();

        switch ($media_type) {
            case 1: // PHOTO
            case 2: // VIDEO
                if (!empty($item->getImageVersions2()) && $item->getImageVersions2()->isCandidates())
                    return $item->getImageVersions2()->getCandidates()[0]->getUrl();
                break;
            case 8: // ALBUM
                if ($item->isCarouselMedia())
                    if (!empty($item->getCarouselMedia()[0]->getImageVersions2() && $item->getCarouselMedia()[0]->getImageVersions2()->isCandidates()))
                        return $item->getCarouselMedia()[0]->getImageVersions2()->getCandidates()[0]->getUrl();
                break;
        }
        return null;
    }

    /**
     * @param $targetId
     * @return int
     */
    private function GetCommentCount($targetId): int
    {
        $data = $this->database->get('post', [
            'comment_count'
        ], [
            'id' => $targetId
        ]);
        if (empty($data)) {
            return 0;
        } else {
            return $data['comment_count'];
        }
    }

    /**
     * @param string $mediaId
     * @param \InstagramAPI\Response\Model\Item $item
     * @param int $idNewPost
     * @param int $userId
     * @return int
     */
    private function FindAndAddPost($mediaId, $item, $idNewPost, $userId)
    {
        $targetId = $this->findMediaByPk($mediaId);
        if ($targetId === 0) {
            $takenAt = $item->getTakenAt();
            if ($item->isCaption()) {
                $caption = $item->getCaption()->getText();
            } else {
                $caption = '';
            }
            //$caption = mb_strimwidth($caption, 1, 100);
            $url = $this->GetMediaUrlFirst($item);
            $commentCount = $item->getCommentCount();
            $data = $this->database->insert('post', [
                'pk' => $mediaId,
                'caption' => $caption,
                'new_post_id' => $idNewPost,
                'taken_at' => $takenAt,
                'account_id' => $userId,
                'image_url' => $url,
                'comment_count' => $commentCount
            ]);
            $this->logger->debug("FindAndAddPost. Insert. Row count: " . $data->rowCount());
            $targetId = $this->database->id();
        }
        return $targetId;
    }

    /**
     * @param \InstagramAPI\Response\Model\Comment $comment
     * @param int $postId
     */
    private function AddComment($comment, $postId)
    {
        $user = $comment->getUser();
        $text = $comment->getText();
        $pk = $comment->getPk();
        $created_at = $comment->getCreatedAt();

        $account_id = $this->FindAndAddAccount($user);

        $data = $this->database->insert('comments', [
            'pk' => $pk,
            'caption' => $text,
            'post_id' => $postId,
            'account_id' => $account_id,
            'created_at' => $created_at
        ]);
        $this->logger->debug("AddComment. Insert. Row count: " . $data->rowCount());
    }

}