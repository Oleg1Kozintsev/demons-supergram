<?php
/**
 * Класс для расчёта статистики по аудитории и сохранения расчитанных данных в базу данных
 */

namespace demonsThebloggers\Actions;

use Medoo\Medoo;
use kozintsev\ALogger\Logger;

class audienceCalc
{
    const sql_base = <<<SQL
SELECT COUNT(*)
FROM stats_followers INNER JOIN stats_accounts ON stats_followers.follower_id = stats_accounts.id AND stats_followers.user_id = :user_id
SQL;

    /** username by instagram
     * @var string
     */
    private $id;

    /**
     * @var Medoo
     */
    private $database;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var array
     */
    private $startWhere;

    public function __construct(Medoo $database, $user_id, $log_file)
    {
        $this->database = $database;
        $this->id = $user_id;
        $this->startWhere = [
            'accounts_id' => $this->id
        ];
        $this->logger = new Logger($log_file, \Psr\Log\LogLevel::INFO);
    }

    /**
     * @throws \Exception - Number of people is less than zero
     */
    public function calcAudience(){
        echo "Calc Audience \n";
        $where = <<<SQL
WHERE  (media_count < 15 AND media_count IS NOT NULL) OR is_404 = 1
SQL;
        $sql = self::sql_base . PHP_EOL . $where;

        $people_others = $this->collectionOfAudience("empty", 'audience', $sql);

        $where = <<<SQL
WHERE followings_count > 1000 AND followings_count IS NOT NULL
SQL;
        $sql = self::sql_base . PHP_EOL . $where;

        $people_others += $this->collectionOfAudience("spam", 'audience', $sql);

        $where = <<<SQL
WHERE is_business = 1 AND is_business IS NOT NULL
SQL;
        $sql = self::sql_base . PHP_EOL . $where;

        $people_others += $this->collectionOfAudience("business", 'audience', $sql);

        $people_all = $this->getPeopleCount();

        $people = $people_all - $people_others;

        if ($people < 0) throw new \Exception("Number of people is less than zero");

        $this->_collectionOfAudience("people", "audience", $people);
    }

    public function calcReach(){
        echo "Calc reach \n";
        $where = <<<SQL
WHERE  followings_count < 300 AND followings_count IS NOT NULL
SQL;
        $sql = self::sql_base . PHP_EOL . $where;

        $this->collectionOfAudience("High", 'reach', $sql);

        $where = <<<SQL
WHERE  followings_count > 300 AND followings_count < 1000  AND followings_count IS NOT NULL
SQL;
        $sql = self::sql_base . PHP_EOL . $where;

        $this->collectionOfAudience("middle", 'reach', $sql);

        $where = <<<SQL
WHERE  followings_count > 1000 AND followings_count IS NOT NULL
SQL;
        $sql = self::sql_base . PHP_EOL . $where;

        $this->collectionOfAudience("low", 'reach', $sql);
    }

    public function calcPopularityOfFollowing(){
        echo "Calc PopularityOfFollowing \n";
        $where = <<<SQL
WHERE  followings_count < 500 AND followings_count IS NOT NULL
SQL;
        $sql = self::sql_base . PHP_EOL . $where;

        $this->collectionOfAudience("500", 'popularityOfFollowing', $sql);

        for ($i = 500 ; $i < 7500; $i = $i + 500){
            $j = $i + 500;
            $where = <<<SQL
WHERE  followings_count > {$i} AND followings_count < {$j} AND followings_count IS NOT NULL
SQL;
            $sql = self::sql_base . PHP_EOL . $where;
            $this->collectionOfAudience((string) $j, 'popularityOfFollowing', $sql);
        }

        $where = <<<SQL
WHERE  followings_count > 8000 AND followings_count IS NOT NULL
SQL;
        $sql = self::sql_base . PHP_EOL . $where;

        $this->collectionOfAudience("8000", 'popularityOfFollowing', $sql);
    }

    public function calcPopularityOfFollowers()
    {
        echo "Calc PopularityOfFollowers \n";
        // переменная часть выражения
        $where = <<<SQL
WHERE  followers_count < 10 AND followers_count IS NOT NULL
SQL;
        $sql = self::sql_base . PHP_EOL . $where;

        $this->collectionOfAudience("<10", 'popularityOfFollowers', $sql);

        $where = <<<SQL
WHERE  followers_count > 10 AND followers_count < 100 AND followers_count IS NOT NULL
SQL;
        $sql = self::sql_base . PHP_EOL . $where;

        $this->collectionOfAudience("<100", 'popularityOfFollowers', $sql);

        $where = <<<SQL
WHERE  followers_count > 100 AND followers_count < 1000 AND followers_count IS NOT NULL
SQL;
        $sql = self::sql_base . PHP_EOL . $where;

        $this->collectionOfAudience("<1000", 'popularityOfFollowers', $sql);

        $where = <<<SQL
WHERE  followers_count > 1000 AND followers_count < 10000 AND followers_count IS NOT NULL
SQL;
        $sql = self::sql_base . PHP_EOL . $where;

        $this->collectionOfAudience("<10k", 'popularityOfFollowers', $sql);

        $where = <<<SQL
WHERE  followers_count > 10000 AND followers_count < 100000 AND followers_count IS NOT NULL
SQL;
        $sql = self::sql_base . PHP_EOL . $where;

        $this->collectionOfAudience("<100k", 'popularityOfFollowers', $sql);

        $where = <<<SQL
WHERE  followers_count > 10000 AND followers_count < 100000 AND followers_count IS NOT NULL
SQL;
        $sql = self::sql_base . PHP_EOL . $where;

        $this->collectionOfAudience("<1M", 'popularityOfFollowers', $sql);

        $where = <<<SQL
WHERE  followers_count > 100000 AND followers_count IS NOT NULL
SQL;
        $sql = self::sql_base . PHP_EOL . $where;

        $this->collectionOfAudience(">=1M", 'popularityOfFollowers', $sql);
    }

    private function getPeopleCount(){
        echo "Calc getPeopleCount \n";
        // переменная часть выражения
        $where = <<<SQL
WHERE  followers_count IS NOT NULL
SQL;
        $sql = self::sql_base . PHP_EOL . $where;

        $rows = $this->database->query(
            $sql, [
                ":user_id" => $this->id
            ]
        )->fetchAll();

        $result = $rows[0][0];

        return $result;
    }

    /**
     * @param string $name
     * @param string $type
     * @return int
     */
    private function getRecords($name, $type) : int
    {
        $where = array_merge($this->startWhere, [
            'name' => $name,
            'type' => $type
        ]);
        $data = $this->database->get('audience', ['id'], $where);
        if (empty($data)) {
            return 0;
        } else {
            return $data['id'];
        }
    }

    /**
     * @param string $name
     * @param string $type
     * @param string $sql
     * @return int
     */
    private function collectionOfAudience($name, $type, $sql)
    {
        $rows = $this->database->query(
            $sql, [
                ":user_id" => $this->id
            ]
        )->fetchAll();

        $result = $rows[0][0];
        return $this->_collectionOfAudience($name, $type, $result);
    }

    /**
     * @param string $name
     * @param string $type
     * @param int $result
     * @return int
     */
    private function _collectionOfAudience($name, $type, $result){
        $id = $this->getRecords($name, $type);
        $where = array_merge($this->startWhere, [
            'name' => $name,
            'type' => $type
        ]);
        if ($id === 0) {
            $data = $this->database->insert('audience', array_merge($where,[
                'value' => $result
            ]));
            $c = $data->rowCount();
            echo "Insert {$c} records. Name {$name} Value {$result} Type {$type} \n";
        } else {
            $data = $this->database->update('audience', [
                'value' => $result
            ], $where);
            $c = $data->rowCount();
            echo "Update {$c} records. Name {$name} Value {$result} Type {$type} \n";
        }
        return $result;
    }

}