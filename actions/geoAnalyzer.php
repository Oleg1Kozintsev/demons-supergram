<?php

namespace demonsThebloggers\Actions;

use GuzzleHttp\Client;

class geoAnalyzer
{
    const username = 'o.kozintsev';

    /** @var string */
    private $url;

    /**  @var Client */
    private $client;

    /** @var string */
    private $json;

    /** @var string */
    private $countryName;

    /** @var string */
    private $cityName;

    /** @var string */
    private $countryCode;

    /**
     * geoAnalyzer constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**  @return string */
    public function getCountryName() : string
    {
        return $this->countryName;
    }

    /**
     * @return string
     */
    public function getCityName(): string
    {
        return $this->cityName;
    }

    /**
     * @return string
     */
    public function getCountryCode() : string
    {
        return $this->countryCode;
    }

    /**
     * Parse json response
     */
    public function parseJson(){
        $geo = json_decode($this->json);
        $this->countryName = $geo->geonames[0]->countryName;
        $this->cityName = $geo->geonames[0]->name;
        $this->countryCode = $geo->geonames[0]->countryCode;
    }

    /**
     * @param string $json
     */
    public function setJson($json)
    {
        $this->json = $json;
    }

    /**
     * @param double $lat
     * @param double $lng
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getPlace($lat, $lng){
        $this->url = sprintf('http://api.geonames.org/findNearbyPlaceNameJSON?lat=%f&lng=%f&username=%s', $lat, $lng, self::username);
        $res = $this->client->request('GET', $this->url);
        echo $res->getStatusCode() . "\n";
        // Must be "200"
        $arr = $res->getHeader('content-type');
        echo $arr[0] . "\n";
        // 'application/json; charset=utf8'
        $this->json = $res->getBody();
        echo $this->json . "\n";
    }
}