<?php
namespace demonsThebloggers\Actions;

use Medoo\Medoo;

class Liker
{
    /**
     * @var int
     */
    private $userIdDb;
    /**
     * @var Medoo
     */
    private $database;
    /**
     * @var \InstagramAPI\Instagram
     */
    private $ig;
    /**
     * @var string
     */
    private $message = '';
    /**
     * @var bool
     */
    private $error = false;

    /**
     * @param int $user_id
     * @param string $media_id
     */
    private function add_in_db($user_id, $media_id){
        $this->database->insert('statistic_like', [
            'user_id' => $user_id,
            'media_id' => $media_id,
        ]);
    }

    private function find_like($user_id, $media_id) : bool {
        $data = $this->database->select('statistic_like', [
            'id'
        ], [
            'user_id' => $user_id,
            'media_id' => $media_id
        ]);
        echo json_encode($data);
        if(empty($data)){
            return false;
        } else {
            return true;
        }
    }

    /**
     * accountAnalyzer constructor.
     * @param Medoo $database
     * @param \InstagramAPI\Instagram $ig
     * @param int $userIdDb
     */
    public function __construct(Medoo $database, \InstagramAPI\Instagram $ig, $userIdDb){
        $this->userIdDb = $userIdDb;
        $this->database = $database;
        $this->ig = $ig;
    }

    public function like_popular_feed(){
        try{
            $feed = $this->ig->discover->getPopularFeed();
            // The getPopularFeed() has an "items" property, which we need.
            $items = $feed->getItems();
            // Individual item objects have an "id" property.
            foreach ($items as $item) {
                $mediaId = $item->getId();
                $isLike = $this->find_like($this->userIdDb, $mediaId);
                if (!$isLike) {

                    $this->ig->media->like($mediaId);
                    $this->add_in_db($this->userIdDb, $mediaId);
                }
                sleep(2);
            }
            $this->message = 'Items count :' . count($items);
        } catch (Exception $e){
            $this->error = true;
            $this->message = $e->getMessage();
        }
    }

    public function likes_for_user($userId, $count){
        try{
            $feed = $this->ig->timeline->getUserFeed($userId);
            $items = $feed->getItems();
            $i = 0;
            // Individual item objects have an "id" property.
            foreach ($items as $item) {
                $mediaId = $item->getId();
                $isLike = $this->find_like($this->userIdDb, $mediaId);
                if (!$isLike) {
                    $this->ig->media->like($mediaId);
                    //$this->ig->media->getLikers($mediaId);
                    $this->add_in_db($this->userIdDb, $mediaId);
                }
                sleep(2);
                $i++;
                if ($i >= $count)
                    break;
            }
            $this->message = 'Items count :' . count($items);
        } catch (Exception $e){
            $this->error = true;
            $this->message = $e->getMessage();
        }
    }

    public function getMessage() : string {
        return $this->message;
    }

    public function isError() : bool {
        return $this->error;
    }

    public function getLikersAll($mediaId){
        try {
            $this->ig->media->getInfo($mediaId);
            $this->ig->media->getLikedFeed($mediaId);
            $this->ig->media->getLikers($mediaId);
            // Starting at "null" means starting at the first page.
            $maxId = null;
            do {
                $response =$this->ig->media->getLikedFeed($maxId);//getUserFeed($userId, $maxId);
                foreach ($response->getItems() as $item) {
                    printf("[%s] https://instagram.com/p/%s/\n", $item->getId(), $item->getCode());
                }
                $maxId = $response->getNextMaxId();
                echo "Sleeping for 5s...\n";
                sleep(5);
            } while ($maxId !== null); // Must use "!==" for comparison instead of "!=".
        } catch (\Exception $e) {
            echo 'Something went wrong: '.$e->getMessage()."\n";
        }
    }

}