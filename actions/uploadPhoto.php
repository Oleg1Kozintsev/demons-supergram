<?php
namespace demonsThebloggers\Actions;

use Exception;
use InstagramAPI\Instagram;

class UploadPhoto // extends Threaded implements Collectable
{
    private $username;
    private $password;
    private $photo;     // path to the photo
    private $caption;     // caption
    private $completed = false;
    private $error = false;
    private $message = '';
    private $proxyUrl = null;


    public function __construct($username, $password, $photo, $caption)
    {
        $this->username = $username;
        $this->password = $password;
        $this->photo = $photo;
        $this->caption = $caption;
    }

    public function setProxy($proxyUrl)
    {
        $this->proxyUrl = $proxyUrl;
    }

    public function run()
    {
        $ig = new Instagram(true, false);

        if (isset($this->proxyUrl))
        {
            $ig->setProxy($this->proxyUrl);
        }

        try {
            $ig->login($this->username, $this->password);
        } catch (Exception $e) {
            $this->error = true;
            $this->message = $e->getMessage();
            $this->completed = true;
            return;
        }

         $metadata = ['caption' => $this->caption];
         // todo: размер фото не более 1080 х 1350
         // нужно предусмотреть проверку и преобразование размера фото
         try {
             $ig->timeline->uploadPhoto($this->photo, $metadata);
         } catch (Exception $e) {
             $this->error = true;
             $this->message = $e->getMessage();
         }

        $this->completed = true;
    }


    public function isCompleted(): bool {
        return $this->completed;
    }

    public function isError() : bool {
        return $this->error;
    }
    
    public function getMessage() : string {
        return $this->message;
    }

}