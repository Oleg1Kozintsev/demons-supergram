<?php
//Игнорировать обрыв связи с браузером
ignore_user_abort(1);
//Время работы скрипта неограничено
set_time_limit(0);

require_once("vendor/autoload.php");
require_once("config/db.php");

use Medoo\Medoo;
use kozintsev\ALogger\Logger;
use demonsThebloggers\Actions\audienceCalc;

$log_file = __DIR__ . '/log/update-cache.log';

$logger = new Logger($log_file, \Psr\Log\LogLevel::INFO);

$database = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $db_name,
    'server' => $db_server,
    'username' => $db_user,
    'password' => $db_pass,
    'charset' => 'utf8',
]);

// ID buzova86
$user_id = 1153;

$calc = new audienceCalc($database, $user_id, $log_file);
$logger->info('start calcPopularityOfFollowing');
$calc->calcPopularityOfFollowing();
$logger->info('start calcPopularityOfFollowers');
$calc->calcPopularityOfFollowers();
$logger->info('start calcReach');
$calc->calcReach();
$logger->info('start calcAudience');
$calc->calcAudience();
$logger->info('End');