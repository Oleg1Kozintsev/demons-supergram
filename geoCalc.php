#!/usr/bin/php
<?php
/**
 * @author Oleg Kozintsev
 * @link
 * скрипт для получение geo данных
 */
//Игнорировать обрыв связи с браузером
ignore_user_abort(1);
//Время работы скрипта неограничено
set_time_limit(0);
date_default_timezone_set('UTC');

require_once ("vendor/autoload.php");
require_once ("config/db.php");

use Medoo\Medoo;
use demonsThebloggers\Actions\geoAnalyzer;

// Initialize
$database = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $db_name,
    'server' => 'localhost',
    'username' => $db_user,
    'password' => $db_pass,
    'charset' => 'utf8'
]);

$rows = $database->select("stats_accounts", [
    "id", "username", "latitude", "longitude",
], [
    "is_sync" => 1,
    'is_error' => 0,
    'latitude[!]' => [0, null]
] );

$analyzer = new geoAnalyzer();

echo "Rows count = " . count($rows). "\n";

/**
 * @param Medoo $database
 * @param geoAnalyzer $analyzer
 * @param $id
 * @param $lat
 * @param $lng
 */
function UpdateGeoStats(Medoo $database, $analyzer, $id, $lat, $lng){
    try {
        $analyzer->getPlace($lat, $lng);
    } catch (\GuzzleHttp\Exception\GuzzleException $e) {
        echo "GuzzleException: " . $e->getMessage();
        return;
    }
    $analyzer->parseJson();
    $database->update('stats_accounts',[
        'country' => $analyzer->getCountryName(),
        'city' => $analyzer->getCityName(),
        'countryCode' => $analyzer->getCountryCode()
    ], [
        'id' => $id
    ]);
    echo "Country: " . $analyzer->getCountryName(). " City:". $analyzer->getCityName() . "\n";
}

foreach ($rows as $item) {
    UpdateGeoStats($database, $analyzer, $item['id'], $item['latitude'], $item['longitude']);
    sleep(5);

}

$rows = $database->select("stats_accounts", [
    "id", "username", "latitude", "longitude",
], [
    "is_sync" => 1,
    'is_error' => 0,
    'country' => null
] );

if (count($rows) == 0) {
    echo "Rows count = 0 \n";
    return;
}

echo "Rows count = " . count($rows). "\n";

foreach ($rows as $item) {
    $id = $item['id'];
    $posts = $database->select('stat_post', ['location'], ['instagram_id' => $id , "location[!]" => null, "LIMIT" => 1]);
    foreach ($posts as $post) {
        $location = json_decode($post['location']);
        UpdateGeoStats($database, $analyzer, $id, $location->lat, $location->lng);
        sleep(5);
    }

}

echo "Finish \n";