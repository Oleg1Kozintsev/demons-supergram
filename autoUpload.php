#!/usr/bin/php
<?php
//Игнорировать обрыв связи с браузером
ignore_user_abort(1);
//Время работы скрипта неограничено
set_time_limit(0);

require_once("vendor/autoload.php");
require_once("lib/simple_html_dom.php");
require_once("utilities/Downloader.php");

require_once("config/user-worldofbeautyful.php");

use kozintsev\ALogger\Logger;
use demonsThebloggers\Models\Proxy;
use demonsThebloggers\Actions\UploadPhoto;

$host = "https://www.goodfon.ru";

$photo_folder = __DIR__. '/';
$log_file = __DIR__. '/log/auto-upload.log';

$logger = new Logger($log_file, \Psr\Log\LogLevel::DEBUG);
$logger->info('Auto upload start');

$start_url = $host . "/catalog/nature/";
$html = Downloader::get($start_url);

$content = str_get_html($html);
// первая картинка /html/body/div[1]/div[11]/div/a вторая /html/body/div[1]/div[12]/div/a
$elems = $content->find("/html/body/div[2]/div[4]/div[1]/div/a");
//<a href="/wallpaper/priroda-leto-pole-lavanda-solntse.html" title="Природа, лаванда, поле, солнце, лето"
foreach($elems as $a){
    $msg = $a->title; // текст пишем в инсаграмм пост
    $msg = mb_strtolower($msg);
    $pos1 = strripos($msg, 'природа');
    $pos2 = strripos($msg, 'nature');
    $msg = str_replace(' ', '', $msg);
    $msg = str_replace(', ...', '',  $msg);
    $msg = str_replace(',', ' #',  $msg);
    $msg = str_replace('-', '', $msg);
    $msg = $a->title . " #" . $msg;
    if ($pos1 === false) {
        // не найдено
        $msg .= ' #природа';
    }
    if ($pos2 === false) {
        $msg .= ' #nature';
    }

    $photo_name = str_replace('.html', '', $a->href);
    $photo_name = str_replace($host, '', $photo_name);
    $photo_name = str_replace("/wallpaper/", '', $photo_name); // priroda-leto-pole-lavanda-solntse 
    $html = Downloader::get($host . $a->href, $start_url);
    //https://www.goodfon.ru/download/priroda-leto-pole-lavanda-solntse/1080x960/
    //https://img1.goodfon.ru/original/1080x960/0/1e/more-bereg-pesok-nebo-oblaka.jpg
    // 1 step https://www.goodfon.ru/download/muzhchina-rybak-plyazh-volny/1600x900/
    // 2 step //*[@id="im"]/img
    $photo_page = $host . "/download/" . $photo_name . "/1080x960/";
    $html = Downloader::get($photo_page, $host . $a->href);
    $c = str_get_html($html);
    $es = $c->find("//*[@id=\"im\"]/img");
    if (count($es) == 0){
        echo 'Photo not found', "\n";
        $logger->info('Photo not found');
    }
    foreach($es as $e){
        $img_url = $e->src;
        echo $img_url ."\n";     
        $data = Downloader::get($img_url);
        $filename = uniqid() . "-g.jpg"; // имя файла которе будем отправлять
        $photo = $photo_folder . $filename;
        try {
		    file_put_contents($photo, $data);
        } catch (Exception $e) {
            $logger->error('Could not save file');
            break;
        }
        try {
            $file_size = filesize($photo);
        } catch (Exception $e) {
            echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
            $file_size = 0;
        }
        if ($file_size == 0){
            $logger->error('Image size is zero');
            break;
        }
        $logger->info('Photo file size: ' . $file_size);
		$pause = 30 * rand(1, 100);
        $logger->info('Pause: '. $pause);
        echo "Sleep ... " . $pause . " sec.\n";
		//sleep($pause);

        $uploadPhoto = new UploadPhoto($username, $password, $photo, $msg);
        $proxy = new Proxy();
        $proxy->config($proxyCfg);
        //$uploadPhoto->setProxy($proxy->getUrl());
        $uploadPhoto->run();
        if($uploadPhoto->isError()){
            $err_msg = $uploadPhoto->getMessage();
            $logger->error($err_msg);
        } else {
            $logger->info('Upload Photo complete without errors');
        }
        try{
            unlink($photo); // удаляем файл после отправки
        }catch (Exception $e) {
            echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
        }
		break;
    }
    break;
}
