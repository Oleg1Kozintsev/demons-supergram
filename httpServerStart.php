<?php
/**
 * Created by PhpStorm.
 * @author Oleg Kozintsev <dev@aitool.net>
 * Date: 15.08.2018
 * Time: 20:08
 */

set_time_limit(0);
date_default_timezone_set('UTC');
require __DIR__.'/vendor/autoload.php';

require_once("config/post-analyzer.php");

use Medoo\Medoo;
use demonsThebloggers\Models\Proxy;
use demonsThebloggers\Server\RealtimeHttpServer;

$log_file = __DIR__. '/log/giveaway.log';

echo "Getwinner Http Server Start\n";

// Initialize
$database = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $db_name,
    'server' => $db_server,
    'username' => $db_user,
    'password' => $db_pass,
    'charset' => 'utf8',
]);

echo "Database init. \n";

$logger = new kozintsev\ALogger\Logger($log_file, \Psr\Log\LogLevel::DEBUG);

$ig = new \InstagramAPI\Instagram($debug, false);
try {
    if (isset($proxyCfg) && $proxyCfg != [])
    {
        $proxy = new Proxy();
        $proxy->config($proxyCfg);
        $ig->setProxy($proxy->getUrl());
    }
    $ig->login($username, $password);
} catch (\Exception $e) {
    echo 'Something went wrong: '.$e->getMessage()."\n";
    $logger->error('Something went wrong: '.$e->getMessage());
    return;
}

echo "Instagram init. \n";

// Create main event loop.
$loop = \React\EventLoop\Factory::create();

// Create HTTP server along with Realtime client.
$httpServer = new RealtimeHttpServer($loop, $ig, $database, $logger);
// Run main loop.

echo "Run main loop. \n";

$loop->run();
