<?php
/**
 * @author Oleg Kozintsev
 * @link
 * скрипт для анализа пользователей в инстаграмме (получение фоловеров)
 */
//Игнорировать обрыв связи с браузером
ignore_user_abort(1);
//Время работы скрипта неограничено
set_time_limit(0);

require_once __DIR__. "/vendor/autoload.php";
require_once __DIR__. "/config/db.php";

require_once("config/user-nina.php");

use Medoo\Medoo;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use demonsThebloggers\Models\Proxy;
use demonsThebloggers\Actions\accountAnalyzer;

// Initialize
$database = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $db_name,
    'server'   => $db_server,
    'username' => $db_user,
    'password' => $db_pass,
    'charset' => 'utf8',
]);

$log_file = __DIR__. '/log/analyzer.log';
$logger = new Logger('audience-analyzer');
$logger->pushHandler(new StreamHandler($log_file, Logger::INFO));

function getOutOfLoop(){
    $b = false;
    try {
        $handle = fopen(__DIR__. "/audience.txt", 'r+');
        while (!feof($handle)) {
            $buffer = fgets($handle, 4096);
            $pos    = strripos($buffer, "stop");
            if ($pos === false) {
                $b = false; // not found
                break;
            } else {
                $b = true;
                break;
            }
        }
        fclose($handle);
    } catch (Exception $e) {
        echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
        $b = true;
        $handle = null;
    }
    return $b;
}

function RunTasks($username, $password, $proxyCfg, Medoo $database, $log_file, $maxId = null)
{
    $logger = new Logger('audience-analyzer');
    $logger->pushHandler(new StreamHandler($log_file, Logger::INFO));
    $c = $database->count("stats_accounts");
    echo "Accounts: " . $c . "\n";

    $ig = new \InstagramAPI\Instagram(false, false);
    try {
        if (isset($proxyCfg) && $proxyCfg != []) {
            $proxy = new Proxy();
            $proxy->config($proxyCfg);
            $ig->setProxy($proxy->getUrl());
        }
        $ig->login($username, $password);
    } catch (\Exception $e) {
        echo 'Login ... Something went wrong: ' . $e->getMessage() . "\n";
        $logger->error('Login .... Something went wrong: ' . $e->getMessage());
        return null;
    }

    //$username = 'worldofbeautyful';
    //$username = 'tetyamotya';
    $username = 'buzova86';

    echo "Get followers by " . $username . "\n";

    $a = new accountAnalyzer($database, $ig, ['username' => $username], $log_file);
    $logger->info('Get followers start ... username: ' . $username . ' Accounts: ' . $c);

    if ($a->getPk() === 0){
        $logger->error("Instagram user id = 0");
        return null;
    }

    if ($a->getFollowers(function (accountAnalyzer $a, Logger $logger) {
            if (getOutOfLoop() === true){
                $logger->info('Stop. MaxId: ' . $a->getMaxId());
                return true;
            }
            return getOutOfLoop();
        }, $maxId, false) == false){
        $logger->error('getFollowers message: ' . $a->getMessage() . ' MaxId: ' . $a->getMaxId());
        return $a->getMaxId();
    }
    $logger->info('Get followers finish');
    echo "Get followers by " . $username . ". Finish! \n";
    return null;
}

$maxId = null;
$maxId = "AQDRTAydVU8kxr_xjJbd05S-KX4JTyFdEHYmQsj9WTw1UXgUHLXON88LUicdLVpimF1BnIGdscvf6L4PRkJOEtylZuEWuK0sDr1B8U03ymYJiA"; // последний Id
$maxId = "AQCafF7FeCq4jnuwqVrATjTP1bT2hst8D30Rk2tISaVM_zntqtS7FGtAYHLF5fqPJoKDYyGP_IKk2Kg09IagnXnIRHRO0vTHESooO9ORPcmOiQ";

// перезапусе службы получения списка фоловеров
while (1) {
    // Тут будет располагаться код Демона
    if (getOutOfLoop()) {
        $logger->info('Stop loop audience');
        break;
    }
    $maxId = RunTasks($username, $password, $proxyCfg, $database, $log_file, $maxId);
    if ($maxId === null) {
        break;
    } else {
        $logger->info('Restart after 15 min ....');
        sleep(900);
        $logger->info('Restart');
        continue;
    }
}

$logger->info('Exit get followers');
print "Exit";
