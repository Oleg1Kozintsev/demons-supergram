<?php
require_once("../vendor/autoload.php");

use demonsThebloggers\Models\Proxy;
use PHPUnit\Framework\TestCase;

class proxy_test extends TestCase
{
    function testUrl() {
        $proxy = new Proxy();
        $proxy->setIporhost("185.22.175.225");
        $proxy->setPort("3128");
        $proxy->setLogin("user");
        $proxy->setPassword("pass");
        $proxy->setType("http");
        $url = $proxy->getUrl();
        $this->assertEquals($url, 'http://user:pass@185.22.175.225:3128');
    }
}