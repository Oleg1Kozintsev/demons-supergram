<?php
/**
 * Created by PhpStorm.
 * User: okozi
 * Date: 24.08.2018
 * Time: 19:54
 */
require_once("../vendor/autoload.php");

use demonsThebloggers\Actions\winnerCalc;
use PHPUnit\Framework\TestCase;
use Medoo\Medoo;

class winnerCalcTest extends TestCase
{

    public function testGetWinnerId()
    {
        $database = new Medoo([
            'database_type' => 'mysql',
            'database_name' => 'giveaway',
            'server' => "127.0.0.1",
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ]);

        $calc = new winnerCalc($database, null);
        $res = $calc->GetWinnerId(2, 1534535999, 0);
        $this->assertNotEquals($res, 0);
    }

    public function testGetWinnerIdType1()
    {
        $database = new Medoo([
            'database_type' => 'mysql',
            'database_name' => 'giveaway',
            'server' => "127.0.0.1",
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ]);

        $calc = new winnerCalc($database, null);
        $res = $calc->GetWinnerId(2, 1534535999, 1);
        $this->assertEquals($res, 0);
    }
}
