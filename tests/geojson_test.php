<?php
require_once ("../vendor/autoload.php");
require_once ("../actions/geoAnalyzer.php");

use PHPUnit\Framework\TestCase;

class geo_json_test_test extends TestCase
{
    function testJson() {
        $body = '{"geonames":[{"adminCode1":"48","lng":"37.5","distance":"0","geonameId":502971,"toponymName":"Ramenki","countryId":"2017370","fcl":"P","population":130000,"countryCode":"RU","name":"Ramenki","fclName":"city, village,...","countryName":"Russia","fcodeName":"section of populated place","adminName1":"Moscow","lat":"55.7","fcode":"PPLX"}]}';

        $a = new geoAnalyzer();
        $a->setJson($body);
        $a->parseJson();
        $countryName = $a->getCountryName();
        $city = $a->getCityName();

        $this->assertEquals($countryName, 'Russia');
        $this->assertEquals($city, 'Ramenki');
    }

    function testConnect() {
        $lat = 55.7;
        $lng = 37.5;
        $username = 'o.kozintsev';

        $a = new geoAnalyzer();
        $a->connect($lat, $lng, $username);
        $url = $a->getUrl();

        $this->assertEquals($url, 'Russia');

    }
}