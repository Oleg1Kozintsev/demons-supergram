<?php
require_once("../vendor/autoload.php");

use PHPUnit\Framework\TestCase;

class WebInst_test extends TestCase
{
    public function testParseJson() {
        $str = "{\"user\": {\"pk\": 6173505487, \"username\": \"lyudajyikova_\", \"full_name\": \"\", \"is_private\": true, \"profile_pic_url\": \"https://scontent-arn2-1.cdninstagram.com/t51.2885-19/s150x150/22159198_121187911933099_6488371450133610496_n.jpg\", \"profile_pic_id\": \"1619762111674940289_6173505487\", \"is_verified\": false, \"has_anonymous_profile_picture\": false, \"media_count\": 2, \"follower_count\": 11, \"following_count\": 164, \"biography\": \"\", \"external_url\": \"\", \"usertags_count\": 0, \"hd_profile_pic_versions\": [{\"width\": 320, \"height\": 320, \"url\": \"https://scontent-arn2-1.cdninstagram.com/t51.2885-19/s320x320/22159198_121187911933099_6488371450133610496_n.jpg\"}], \"hd_profile_pic_url_info\": {\"url\": \"https://scontent-arn2-1.cdninstagram.com/t51.2885-19/22159198_121187911933099_6488371450133610496_n.jpg\", \"width\": 428, \"height\": 428}, \"auto_expand_chaining\": false}, \"status\": \"ok\"}";

        $json = json_decode($str);
        $follower_count = $json->user->follower_count;
        $media_count = $json->user->media_count;
        $following_count = $json->user->following_count;
        $username = $json->user->username;
        $this->assertEquals($username, 'lyudajyikova_');
    }

    public function testGetErrorUrl(){
        // bad url
        $url = 'http://i.instagram.com/api/v1/users/617363008/info/';
        $client = new GuzzleHttp\Client();
        try{
            $res = $client->request('GET', $url);
            $status = $res->getStatusCode();

            if ($status === 200) {
                $body = $res->getBody();
            }
        } catch (\Exception $e) {
            $code = $e->getCode();
            $this->assertEquals($e->getCode(), 404);
            if ($code === 404){
                echo 'Bad url';
            }
        }

    }
}