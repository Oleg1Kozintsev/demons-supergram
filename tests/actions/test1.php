#!/usr/bin/php
<?

include __DIR__.'/vendor/autoload.php';


function logger($str){
    $fullname = __DIR__.'/test1.log';
    $fp = fopen($fullname, "a");
    fwrite($fp, $str."\r\n");
    fclose($fp);
}

$db_user = 'root';
$db_pass = '';
$db_name = 'supergram';

$id = $argv[1];

if (is_null($id))
{
    logger("Error argument");
    print "Error argument";
    exit(1);
}


try {
    $dbh = new PDO('mysql:host=localhost;dbname='.$db_name, $db_user, $db_pass);
} catch (PDOException $e) {
    logger("Error!: " . $e->getMessage());
    $dbh = null;
    exit(1);
}

logger("DB OK");

$username = 'dunsolomon';
$password = 'c63255bb2f';

try {
    $i = new \InstagramAPI\Instagram(true, false);
    $i->setUser($username, $password);
    $i->login();
} catch (Exception $e) {
    logger("Error!: " . $e->getMessage());
    $dbh = null;
    exit(1);
}
logger("Instagram OK");

logger('Ok. Param: '.$id);

print 'Ok. Param: '.$id;

$dbh = null;

