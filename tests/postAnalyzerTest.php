<?php
/**
 * Created by PhpStorm.
 * User: okozi
 * Date: 24.08.2018
 * Time: 19:54
 */
require_once("../vendor/autoload.php");

use demonsThebloggers\Actions\postAnalyzer;
use PHPUnit\Framework\TestCase;
use Medoo\Medoo;

class postAnalyzerTest extends TestCase
{

    public function testGetPostInfoFromDb()
    {
        $database = new Medoo([
            'database_type' => 'mysql',
            'database_name' => 'giveaway',
            'server' => "127.0.0.1",
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ]);

        $ig = new \InstagramAPI\Instagram(true, false);

        $pa = new postAnalyzer($database, $ig, null);
        $pa->getPostInfoFromDb("BmMLA5mHxmR");
        $mediaId = $pa->getMediaId();
        $this->assertEquals($mediaId, "1840894788042168721");
    }

}
