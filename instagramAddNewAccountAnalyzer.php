<?php
/**
 * @author Oleg Kozintsev
 * @link
 * скрипт для добавление в анализатор пользователя через UI скрипт должен запускаться в Cron раз минуту
 */
//Игнорировать обрыв связи с браузером
ignore_user_abort(1);
//Время работы скрипта неограничено
set_time_limit(0);
date_default_timezone_set('UTC');

require_once ("vendor/autoload.php");
require_once ("config/db.php");
require_once ("config/main.php");

require_once("config/user-dunsolomon.php");

use Medoo\Medoo;
use kozintsev\ALogger\Logger;
use demonsThebloggers\Models\Proxy;
use demonsThebloggers\Models\MaxAccount;
use demonsThebloggers\Actions\accountAnalyzer;
use demonsThebloggers\Actions\updateCache;


$logger = new Logger($log_file, \Psr\Log\LogLevel::DEBUG);

$cron = True;

// Initialize
$database = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $db_name,
    'server' => $db_server,
    'username' => $db_user,
    'password' => $db_pass,
    'charset' => 'utf8',
]);

/*
 * Что бы анализировать аккаунт нужно добавить его через UI в базу данных
 */
function RunTasks($username, $password, $proxyCfg, Medoo $database, $log_file, Logger $logger){
    $where = [
        "is_sync" => 0,
        "is_error" => 0,
    ];

    $rows_count = $database->count("cache_new_accounts", $where);

    if ($rows_count == 0) {
        echo "Rows count = 0 \n";
        return;
    }


    $ig = new \InstagramAPI\Instagram(true, false);
    try {
        if (isset($proxyCfg) && $proxyCfg != [])
        {
            $proxy = new Proxy();
            $proxy->config($proxyCfg);
            $ig->setProxy($proxy->getUrl());
        }
        $ig->login($username, $password);
    } catch (\Exception $e) {
        echo 'Something went wrong: '.$e->getMessage()."\n";
        $logger->error('Something went wrong: '.$e->getMessage());
        return;
    }

    MaxAccount::$max_followers_count = $database->max('stats_accounts','followers_count');
    MaxAccount::$max_er = $database->get('stats_accounts', 'er', ['followers_count' => MaxAccount::$max_followers_count]);


    $rows = $database->select("cache_new_accounts", [
        "id", "username"
    ], $where );

    foreach ($rows as $item) {
        $a = new accountAnalyzer($database, $ig, $item, $log_file);
        if ($a->getPk() == 0){
            $database->update("cache_new_accounts", [
                "is_error" => 1
            ],[
                "id" => $item["id"]
            ]);
            continue;
        }
        if ($a->GetAccountInfo() == false){
            $logger->error($a->getMessage());
            $database->update("cache_new_accounts", [
                "is_error" => 1
            ],[
                "id" => $item["id"]
            ]);
        } else {
            $database->update("cache_new_accounts", [
                "is_sync" => 1
            ],[
                "id" => $item["id"]
            ]);
            $a->setAccountDoTrue();
            updateCache::updateCacheStatsOneAccount($database, $item["username"], $logger);
            // установить флаг is do
            // скопировать новый аккаунт в кеш
        }
        //break; //- fot faster tests
        sleep(5);
    }

}

/* $is_sync задаёт два режима работы:
1. Запускать скрипт каждую минуту
проверяются записи с флагом is_sync == 0 // такие записи только были добавлены, но не были синхронизированы
для таких записей выполняется получение данных о пользователе
2. 1 раз в день, обновлять информацию для записей у которых is_sync == 1

$is_like_stat задаёт сбор статистики по лайкам в течении суток применяется с $is_sync == 1

Два кейса:
1. каждый час для посто которыс не более 1 суток
2. Раз в день для старых

*/


if ($cron) {
    echo "Script run! \n";
    RunTasks($username, $password, $proxyCfg, $database, $log_file,  $logger);
    echo "Finish! \n";
} else {
     echo "Demon run! \n";
    // Чтобы программа работала постоянно, она просто должна постоянно работать ;)
    while (1) {
        // Тут будет располагаться код Демона
        RunTasks($username, $password, $proxyCfg, $database, $log_file, $logger);
        // Время сна Демона между итерациями (зависит от потребностей системы)
        sleep(10);
    }
    print "Exit";
    exit(0);
}