<?php
/**
 * @author Oleg Kozintsev
 * @link
 * скрипт для анализа пользователей в инстаграмме
 */
//Игнорировать обрыв связи с браузером
ignore_user_abort(1);
//Время работы скрипта неограничено
set_time_limit(0);


require_once __DIR__ . "/vendor/autoload.php";
require_once __DIR__ . "/config/db.php";

require_once("config/user-nina.php");

use Medoo\Medoo;
use kozintsev\ALogger\Logger;
use demonsThebloggers\Models\Post;
use demonsThebloggers\Models\Proxy;
use demonsThebloggers\Actions\accountAnalyzer;

// Initialize
$database = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $db_name,
    'server' => $db_server,
    'username' => $db_user,
    'password' => $db_pass,
    'charset' => 'utf8',
]);

$c = $database->count("invite");
echo "BD test. Ok. C (invite table) = " . $c;

$log_file = __DIR__. '/log/like-grabber.log';
$logger = new Logger($log_file, \Psr\Log\LogLevel::DEBUG, [
    'max_file_size' => 0,
]);


$ig = new \InstagramAPI\Instagram(false, false);
try {
    if (isset($proxyCfg) && $proxyCfg != []) {
        $proxy = new Proxy();
        $proxy->config($proxyCfg);
        $ig->setProxy($proxy->getUrl());
    }
    $ig->login($username, $password);
} catch (\Exception $e) {
    echo 'Something went wrong: ' . $e->getMessage() . "\n";
    $logger->error('Login... Something went wrong: ' . $e->getMessage());
    return;
}

//$username = 'worldofbeautyful';
//$username = 'tetyamotya';
$username = 'buzova86';
//$username = 'lenamyro';
//$username = 'astistudio_';

echo "Get like by " . $username . "\n";

$logger->info("Start like grabber username:" . $username);

$a = new accountAnalyzer($database, $ig, ['username' => $username], $log_file);
// ждём появление нового поста
// сохранеяем его в память
// выполняем получение статистики только по нему

$date = new DateTime();
// получаем текущее время запуска, новый пост должен появится после этого времени
$timestamp = $date->getTimestamp();

$mediaId = 0;
$is_error = false;

if (empty($a->getPk())){
    $logger->error("Instagram user id is empty");
    exit(0);
}

function getOutOfLoop(){
    $b = false;
    try {
        $handle = fopen(__DIR__. "/grabber.txt", 'r+');
        while (!feof($handle)) {
            $buffer = fgets($handle, 4096);
            $pos    = strripos($buffer, "stop");
            if ($pos === false) {
                $b = false; // not found
                break;
            } else {
                $b = true;
                break;
            }
        }
        fclose($handle);
    } catch (Exception $e) {
        echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
        $b = true;
        $handle = null;
    }
    return $b;
}

$logger->info("Loop 1 run");

while (1) {
    if (getOutOfLoop()) {
        $logger->info('Stop (loop 1) likeGrabber');
        break;
    }
    $top = $a->getPostsTop(true, true);
    if ($a->isError()){
        $logger->error($a->getMessage());
        $logger->error("getPostsTop problem.");
        $is_error = true;
        break;
    }
    if (empty($top) && empty($top[0])) {
        echo "Account is closed. Top empty \n";
        $logger->error('Account is closed. Top is empty.');
        $is_error = true;
        break;
    }
    $post = $top[0];
    if (!($post instanceof Post)) {
        $logger->error('Instance not Post');
        $is_error = true;
        break;
    }
    if ($post->media_type != 1){
        // если не фотография, то сделать паузу и продолжить ожидания фотографии
        $logger->debug('Not photo');
        sleep(240);
        continue;
    }
    //$a->collectionOfLikers($post->pk);
    //break;
    if ($post->timestamp > $timestamp) {
        // начинамем записывать
        // сохраняем id поста и выходим
        $mediaId = $post->pk;
        $logger->info("New posts. Media id:" . $mediaId);
        echo "New posts. Media id:" . $mediaId;
        break;
    }
    sleep(120);
}

if (empty($mediaId)) {
    $logger->error('Media id is empty. Exit.');
    exit(1);
}

if($is_error === true){
    exit(1);
}

$logger->info("Loop 2 run");

$j = 0 ;

// Initialize
$database = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $db_name,
    'server' => $db_server,
    'username' => $db_user,
    'password' => $db_pass,
    'charset' => 'utf8',
]);

$logger->debug('Reconnect... DB test connect ...');

$c = $database->count("invite");
echo "BD test. Ok. C (invite table) = " . $c;

if ($c === 0){
    $logger->error('Not connect in database');
    return;
}

$a = new accountAnalyzer($database, $ig, ['username' => $username], $log_file);

// начинаем сохранять статистику по лайкам данного поста
while (1) {
    if (getOutOfLoop()) {
        $logger->info('Stop (loop 2) likeGrabber');
        break;
    }
    if ($j > 10){
        $logger->error('Exit loop 10 error');
        break;
    }
    if ($a->collectionOfLikers($mediaId) === false){
        // ошибка может быть потому что посту удалилил например
        $logger->error($a->getMessage());
        $is_error = true;
        sleep(120);
        $j++;
        continue;
    }    
    sleep(30);
}

$logger->info('Exit');
echo "Exit " . $username . ". Finish! \n";


