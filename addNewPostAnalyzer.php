#!/usr/bin/php
<?php
/**
 * @author Oleg Kozintsev <dev@aitool.net>
 *
 */
//Игнорировать обрыв связи с браузером
ignore_user_abort(1);
//Время работы скрипта неограничено
set_time_limit(0);
date_default_timezone_set('UTC');

require_once ("vendor/autoload.php");
require_once ("config/post-analyzer.php");

use Medoo\Medoo;
use kozintsev\ALogger\Logger;
use demonsThebloggers\Models\Proxy;
use demonsThebloggers\Actions\postAnalyzer;
use demonsThebloggers\Actions\winnerCalc;

$cron = False;

$log_file = __DIR__. '/log/giveaway.log';

// Initialize
$database = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $db_name,
    'server' => $db_server,
    'username' => $db_user,
    'password' => $db_pass,
    'charset' => 'utf8',
]);

$ig = new \InstagramAPI\Instagram($debug, false);
try {
    if (isset($proxyCfg) && $proxyCfg != [])
    {
        $proxy = new Proxy();
        $proxy->config($proxyCfg);
        $ig->setProxy($proxy->getUrl());
    }
    $ig->login($username, $password);
} catch (\Exception $e) {
    echo 'Something went wrong: '.$e->getMessage()."\n";
    $logger->error('Something went wrong: '.$e->getMessage());
    return;
}

$logger = new Logger($log_file, \Psr\Log\LogLevel::INFO);

/**
 * @return bool
 */
function getOutOfLoop() : bool {
    $b = false;
    try {
        $handle = fopen(__DIR__. "/post.txt", 'r+');
        while (!feof($handle)) {
            $buffer = fgets($handle, 4096);
            $pos    = strripos($buffer, "stop");
            if ($pos === false) {
                $b = false; // not found
                break;
            } else {
                $b = true;
                break;
            }
        }
        fclose($handle);
    } catch (Exception $e) {
        echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
        $b = true;
        $handle = null;
    }
    return $b;
}

/**
 * @param \InstagramAPI\Instagram $ig
 * @param Medoo $database
 * @param Logger $logger
 */
function RunTasks($ig, Medoo $database, Logger $logger){
    $where = [
        "is_sync" => 0,
        "is_error" => 0,
    ];

    $rows_count = $database->count("new_post", $where);

    if ($rows_count == 0) {
        //echo "Rows count = 0 \n";
        sleep(10);
        return;
    }

    $rows = $database->select("new_post", [
        "id", "code", "valid_until", "type"
    ], $where );

    $analyzer = new postAnalyzer($database, $ig, $logger);
    $winnerCalc = new winnerCalc($database, $logger);

    foreach ($rows as $item) {
        if ($analyzer->getPostInfo($item['code'], $item['id'])){
            $row = [
                "is_sync" => 1,
                "is_error" => 0,
            ];
            if ($winnerCalc->GetWinnerId($analyzer->getPostId(), $item['valid_until'], $item['type']) ===0){
                $logger->error("The winner is not determined. PostId:" . $analyzer->getPostId());
            }

        } else {
            $row = [
                "is_sync" => 1,
                "is_error" => 1,
            ];
            $logger->error("Sync error. PostId:" . $analyzer->getPostId());
        }
        $database->update("new_post", $row, [
            "id" => $item['id']
        ]);
        //break; //- fot faster tests
        sleep(5);
        // очистка базы данных после определение победителя
        $database->delete("comments", [
                'post_id' => $analyzer->getPostId()
        ]);
    }

}

/* $is_sync задаёт два режима работы:
1. Запускать скрипт каждую минуту
проверяются записи с флагом is_sync == 0 // такие записи только были добавлены, но не были синхронизированы
для таких записей выполняется получение данных о пользователе
2. 1 раз в день, обновлять информацию для записей у которых is_sync == 1

$is_like_stat задаёт сбор статистики по лайкам в течении суток применяется с $is_sync == 1

Два кейса:
1. каждый час для посто которыс не более 1 суток
2. Раз в день для старых

*/


if ($cron) {
    echo "Script run! \n";
    RunTasks($ig, $database, $logger);
    echo "Finish! \n";
} else {
    echo "Demon run! \n";
    // Чтобы программа работала постоянно, она просто должна постоянно работать ;)
    while (1) {
        if (getOutOfLoop()) {
            $logger->info('Stop (loop 2) commentGrabber');
            break;
        }
        // Тут будет располагаться код Демона
        RunTasks($ig, $database, $logger);
        // Время сна Демона между итерациями (зависит от потребностей системы)
        sleep(1);
    }
    print "Exit";
    exit(0);
}