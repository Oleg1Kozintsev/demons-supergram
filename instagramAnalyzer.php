<?php
/**
 * @author Oleg Kozintsev
 * @link
 * скрипт для анализа пользователей в инстаграмме
 */
//Игнорировать обрыв связи с браузером
ignore_user_abort(1);
//Время работы скрипта неограничено
set_time_limit(0);
date_default_timezone_set('UTC');
require_once ("vendor/autoload.php");

require_once ("config/db.php");
require_once ("config/main.php");

use Medoo\Medoo;
use kozintsev\ALogger\Logger;
use demonsThebloggers\Models\MaxAccount;
use demonsThebloggers\Actions\accountAnalyzer;
use demonsThebloggers\Models\Proxy;

$logger = new Logger($log_file, \Psr\Log\LogLevel::DEBUG);

$cron = True;

// Initialize
$database = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $db_name,
    'server' => $db_server,
    'username' => $db_user,
    'password' => $db_pass,
    'charset' => 'utf8',
]);

function SelectPagination(Medoo $database, $ig, $where, $is_sync, Logger $logger, $is_like_stat, $is_data_check , $log_file, $start, $finish){
    $where = array_merge($where, ['LIMIT' => [$start, $finish]]);
    $columns = $is_sync == 1 ? ["pk"] : ["username"];
    $rows = $database->select("stats_accounts",
        $columns, $where );

    foreach ($rows as $item) {
        $a = new accountAnalyzer($database, $ig, $item, $log_file);
        if ($is_sync == 1){
            if ($a->GetAccountInfo($is_like_stat, $is_data_check) == false){
                $logger->error($a->getMessage());
            }
        } else {
            if ($a->SearchAccount()){
                if ($a->GetAccountInfo() == false){
                    $logger->error($a->getMessage());
                }
            } else {
                $a->setAccountError();
                $logger->info('Account '. $item['username'] . ' not found');
            }
        }
        //break; //- fot faster tests
        sleep(5);
    }
}


/*
 * Что бы анализировать аккаунт нужно добавить его через UI в базу данных
 */
function RunTasks($username, $password, $proxyCfg, Medoo $database, $is_sync, $log_file, $is_like_stat, $is_data_check, Logger $logger){
    if ($is_sync == 0) {
        $where = [
            "is_sync" => $is_sync,
            "is_error" => 0,
            "is_do" => 1
        ];
    } else {
        $where = [
            "is_sync" => $is_sync,
            "is_error" => 0,
            "is_do" => 1
        ];
    }
    $rows_count = $database->count("stats_accounts", $where);

    if ($rows_count == 0) {
        echo "Rows count = 0 \n";
        return;
    }


    $ig = new \InstagramAPI\Instagram(false, false);
    try {
        if (isset($proxyCfg) && $proxyCfg != [])
        {
            $proxy = new Proxy();
            $proxy->config($proxyCfg);
            $ig->setProxy($proxy->getUrl());
        }
        $ig->login($username, $password);
    } catch (\Exception $e) {
        echo 'Something went wrong: '.$e->getMessage()."\n";
        $logger->error('Something went wrong: '.$e->getMessage());
        return;
    }

    MaxAccount::$max_followers_count = $database->max('stats_accounts','followers_count');
    MaxAccount::$max_er = $database->get('stats_accounts', 'er', ['followers_count' => MaxAccount::$max_followers_count]);

    $f = 500;
    $c = round($rows_count / $f);
    $m = $rows_count % $f;
    $s = 0;
    for ($i = 1; $i <= $c; $i++) {
            SelectPagination($database, $ig, $where, $is_sync, $logger, $is_like_stat, $is_data_check , $log_file, $s, $f);
        $s = $f;
        $f = $i * 500;
    }
    if ($m != 0){
        SelectPagination($database, $ig, $where, $is_sync, $logger, $is_like_stat, $is_data_check , $log_file, $f, $f + $m);
    }
}

/* $is_sync задаёт два режима работы:
1. Запускать скрипт каждую минуту
проверяются записи с флагом is_sync == 0 // такие записи только были добавлены, но не были синхронизированы
для таких записей выполняется получение данных о пользователе
2. 1 раз в день, обновлять информацию для записей у которых is_sync == 1

$is_like_stat задаёт сбор статистики по лайкам в течении суток применяется с $is_sync == 1

Два кейса:
1. каждый час для посто которыс не более 1 суток
2. Раз в день для старых

*/


if ($cron) {
    echo "Script run! \n";
    RunTasks($username, $password, $proxyCfg, $database, $is_sync, $log_file, $is_like_stat, $is_data_check, $logger);
    echo "Finish! \n";
} else {
     echo "Demon run! \n";
    // Чтобы программа работала постоянно, она просто должна постоянно работать ;)
    while (1) {
        // Тут будет располагаться код Демона
        RunTasks($username, $password, $proxyCfg, $database, $is_sync, $log_file, $is_like_stat, $is_data_check, $logger);
        // Время сна Демона между итерациями (зависит от потребностей системы)
        sleep(10);
    }
    print "Exit";
    exit(0);
}