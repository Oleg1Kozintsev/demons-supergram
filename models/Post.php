<?php
namespace demonsThebloggers\Models;

class Post
{
    /** @var integer  */
    public $like_count;
    /** @var integer  */
    public $comment_count;
    /** @var string  */
    public $url;
    /** @var string */
    public $caption;
    /** @var string  */
    public $pk;
    /** @var integer  */
    public $timestamp;
    /** @var string  */
    public $code;
    /** @var Location  */
    public $location;
    /** @var integer  */
    public $media_type;

}