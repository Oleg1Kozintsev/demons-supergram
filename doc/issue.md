
[Realtime client for receiving new posts](https://github.com/mgp25/Instagram-API/issues/1629 "https://github.com/mgp25/Instagram-API/issues/1629")

Как получать Push уведомления на посты пользователей, на которые ты подписан

http://i.instagram.com/api/v1/users/2100698192/info/

```php
$client = new GuzzleHttp\Client();

$pk = 2100698192;
$proxy = 'http://username:password@192.168.16.1:10';

$client->request('GET', 'http://i.instagram.com/api/v1/users/'. $pk .'/info/', [
    'proxy' => [
        'http'  => $proxy, // Use this
    ]
]);

echo $res->getStatusCode();
// "200"
echo $res->getHeader('content-type');
// 'application/json; charset=utf8'
echo $res->getBody();
// {"type":"User"...'

// Send an asynchronous request.
$request = new \GuzzleHttp\Psr7\Request('GET', 'http://i.instagram.com/api/v1/users/'. $pk .'/info/', [
    'proxy' => [
        'http'  => $proxy, // Use this
    ]
]);
$promise = $client->sendAsync($request)->then(function ($response) {
    echo 'I completed! ' . $response->getBody();
});
$promise->wait();

```


## Скорость

200 пользователей каждые 3 секуныд

для 11 миллионов нужно 46 часов

если получать каждую секунду

15,5 часов

1 пользователь в 1 сек

при 100 процессах нужен 31 час

## Память

При формирования запроса к порльзователям бузовой нужно 

2Gb памяти, если делать эту операцию в памяти

## Память

Диаграммы аудитории храняться к кеширующей таблице cache_audience в NoSQL представлениие в формате JSON

следующие данные:

name - название диаграммы:
возможные варианты: reach (Досягаемость)
формат json:
```json
[
          ['reach', 'Count'],
          ['High',     17771],
          ['middle',      211231],
          ['low',  555566],
          
]
```

```json
[["reach", "Count"],["High",602769],["middle",189712],["low",129361]]
```


```php
 private function collectionOfLikeStatistics($username, $is_data_check): int
    {
        $id = $this->findUserId($username);
        if ($id === 0) {
            echo "User not found \n";
            return 0;
        }
        $top = $this->getPostsTop(true, true);
        if (empty($top) && empty($top[0])) {
            echo "Top empty \n";
            return 0;
        }
        $post = $top[0];
        if (!($post instanceof Post)) {
            return 0;
        }

        $date = new DateTime();
        $timestamp = $date->getTimestamp();


        $row = $this->database->select('stat_post', [
            'taken_at', 'pk'
        ], [
            'id' => $id,
            "ORDER" => ["taken_at" => "DESC"],
            "LIMIT" => 1
        ]);

        if (empty($row)) {
            return 0;
        }
        $last_media_pk = $row[0]['pk'];
        if ($post->pk != $last_media_pk) {
            $this->database->delete('stats_likes_last_post', [
                'instagram_id' => $id
            ]);
        } else {
            $current_media_timestamp = $row[0]['taken_at'];
            if ($current_media_timestamp != 0 && ($timestamp - self::OneDay) > $current_media_timestamp && $is_data_check) {
                echo "Stop not work. One day finish \n";
                return 0;
            }
        }

        $this->database->insert('stats_likes_last_post', [
            'instagram_id' => $id,
            'like_count' => $post->like_count,
            'comment_count' => $post->comment_count,
            'created_at' => $timestamp,
        ]);

        return $id;
    }
```