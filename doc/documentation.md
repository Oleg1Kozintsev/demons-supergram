# Документация для сервисов thebloggers.ru

## Процессы

- instagramAnalyzerEveryDay.php - обновление аккаунта каждый день, так же обновляются топ 20 постов (добавляются новые)
- userdataUpdate.php - обновление информации о пользователе через web интерфейс, без авторизации
- instagramLikeStatEveryDay.php -  ежедневный сбор динамики лайков последнего поста, опубликованного более чем одни сутки назад
- instagramLikeStatEveryHour.php - сбор динамики лайков последнего постав каждый час, для свежего поста, опубликованного в течении суток 
- updateCache.php - скрипт для обновления данных в кеш таблицах
- instagramAddNewAccountAnalyzer.php - обновление только что добавленных аккаунтов

### Расчёты и кеширование

-updateCache.php - обновление видимых на главной странице пользователей
-audienceCalc.php - расчёт значений для диаграмм по посещяемости пока только для пользователя buzova86 (1153)

###Старый процессы:

-instagramUpdate.php сервис для обновления данные для автопостинга, нужен что бы получить данные о аватарке и полном имени у добавленного пользователя, а так же чекнуть этого пользователя на тему коректности
-schedulerService.php - сервис отложенного постинга

Пример запуска процесса в cron:

```bash
php /var/www/user59421/data/demon/updateCache.php
```

## Структура базы данных 

### Таблица cache_new_accounts 

Все добавленные через UI пользователи попадают сюда, затем обрабатываются скриптом и отправляются в stats_accounts и cache_stats_accounts

### Таблица stats_accounts

top_photo - JSON файл содержащий структуру данных эквивалентную models\Posts.php (устарел, удалён)
is_sync - True если из Инстаграмма получены, False - ещё нет
is_error - во время получения данных из Инстаграмма или обновление данных произошла ошибка
is_do - выполнять синхронизацию данного аккаунта всегда, если False не выполнять синхронизацию.
req_auth - если True - то это закрытый аккаунт
edit_location - если True, то локация пользователя промодерирована и не должна менятся (не реализовано)

### Таблица stats_followings

Тесты для buzova86 (id = 1153)

Подписчики (для хранения связей)

Получение ТОП 30 Подписчиков

```SQL
SELECT stats_accounts.id, stats_accounts.username,  stats_accounts.followers_count, stats_accounts.fullname
FROM stats_followers INNER JOIN stats_accounts ON stats_followers.follower_id = stats_accounts.id AND stats_followers.user_id = 1153
ORDER BY stats_accounts.followers_count DESC LIMIT 0, 30;
```


Получение подписчиков для worldofbeautyful

```SQL
SELECT username, followers_count, fullname FROM stats_followers, stats_accounts 
WHERE stats_followers.follower_id = stats_accounts.id AND stats_followers.user_id = 1331
```

При получении списка подписчиков приходит максимум 200 подписчиков


Топ лайкеров поста с ID 1628419767215175001 (пост buzova86)

```SQL
SELECT stats_accounts.id, username, followers_count, followings_count, media_count, fullname
FROM statistics_likers
INNER JOIN stats_accounts ON stats_accounts.id = statistics_likers.user_id
WHERE media_id = 1630352365068223981

```


Такого быть не должно:

```SQL
SELECT * FROM stats_followers WHERE stats_followers.user_id = 1153 AND stats_followers.follower_id = 1153
```

Данный тест должен вернуть пустой результат

Запрос для обновления кешированного топа 30 пользователя

```SQL
DELETE FROM cache_top_followers WHERE user_id = 1153;

INSERT INTO cache_top_followers(user_id, follower_id, username, followers_count, fullname, profilePicUrl)
SELECT stats_followers.user_id, stats_accounts.id, stats_accounts.username,  stats_accounts.followers_count, stats_accounts.fullname, stats_accounts.profilePicUrl
FROM stats_followers INNER JOIN stats_accounts ON stats_followers.follower_id = stats_accounts.id AND stats_followers.user_id = 1153
ORDER BY stats_accounts.followers_count DESC LIMIT 0, 30;
```


### Запрос для формирвания csv файла

```SQL
SELECT stats_accounts.pk, stats_accounts.username, stats_accounts.fullname, stats_accounts.followers_count, 
stats_accounts.followings_count, stats_accounts.media_count, stats_accounts.gender
FROM stats_followers INNER JOIN stats_accounts ON stats_followers.follower_id = stats_accounts.id AND stats_followers.user_id = 1153
LIMIT 0, 500;
```

Обновление таблицы с кешированными аккаунтами:

Удаляем и создаём таблицу с кешированными данными

```SQL
DROP TABLE IF EXISTS `cache_stats_accounts`;

CREATE TABLE `cache_stats_accounts` (
  `id` bigint(20) NOT NULL,
  `username` varchar(45) NOT NULL,
  `followers_count` bigint(20) DEFAULT NULL,
  `followings_count` bigint(20) DEFAULT NULL,
  `media_count` bigint(20) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `biography` varchar(255) DEFAULT NULL,
  `birthday` varchar(45) DEFAULT NULL,
  `latitude` varchar(45) DEFAULT NULL,
  `longitude` varchar(45) DEFAULT NULL,
  `publicphonenumber` varchar(45) DEFAULT NULL,
  `socialcontext` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `profilePicUrl` varchar(255) DEFAULT NULL,
  `pk` bigint(20) DEFAULT NULL,
  `is_sync` tinyint(4) NOT NULL DEFAULT '0',
  `is_error` tinyint(4) NOT NULL DEFAULT '0',
  `is_do` tinyint(4) NOT NULL DEFAULT '1',
  `is_show` tinyint(4) NOT NULL DEFAULT '0',
  `like_avg` bigint(20) DEFAULT '0',
  `comment_avg` bigint(20) DEFAULT '0',
  `rating` bigint(20) DEFAULT '0',
  `er` float DEFAULT '0',
  `lr` float DEFAULT '0',
  `tr` float DEFAULT '0',
  `last_media_pk` bigint(20) DEFAULT '0',
  `last_media_url` varchar(255) DEFAULT '0',
  `last_media_caption` varchar(1024) DEFAULT '0',
  `last_media_timestamp` int(11) DEFAULT '0',
  `last_media_code` varchar(25) DEFAULT '0',
  `country` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `countryCode` varchar(10) DEFAULT NULL,
  `req_auth` tinyint(4) DEFAULT '0',
  `is_business` tinyint(4) DEFAULT '0',
  `is_404` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `pk` (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

Очищаем таблицу:

```SQL
TRUNCATE TABLE `cache_stats_accounts`
```

Добавляем данные из основной таблицы

```SQL
INSERT INTO cache_stats_accounts(id, username, followers_count, followings_count, media_count, fullname, gender, email, biography, birthday,
latitude, longitude, publicphonenumber, socialcontext, created_at, updated_at, profilePicUrl, pk, is_sync, is_error, is_do, is_show, like_avg, 
comment_avg, rating, er, lr, tr, country, city, countryCode,
req_auth, is_business, is_404)
SELECT id, username, followers_count, followings_count, media_count, fullname, gender, email, biography, birthday,
latitude, longitude, publicphonenumber, socialcontext, created_at, updated_at, profilePicUrl, pk, is_sync, is_error, is_do, is_show, like_avg, 
comment_avg, rating, er, lr, tr, country, city, countryCode,
req_auth, is_business, is_404 FROM stats_accounts
WHERE is_do = 1 AND pk is not null
```

Обновление, но синтаксис MariaDB 5.5 не позволяет делать такой запрос

```SQL
UPDATE cache_stats_accounts t1
SET followers_count = t2.followers_count
, followings_count = t2.followings_count
, media_count = t2.media_count
, updated_at = t2.updated_at
, like_avg = t2.like_avg
, comment_avg = t2.comment_avg
, rating = t2.rating
, er = t2.er
, lr = t2.lr
, tr = t2.tr
FROM stats_accounts t2
WHERE (t1.id = t2.id);
```

Необходимо нормализовать данные и убрать поле top_photo

Кеш для топ фото

```SQL
CREATE TABLE `cache_top_followers` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `user_id` BIGINT(20) NOT NULL,
  `follower_id` BIGINT(20) NOT NULL,
  `username` VARCHAR(45) NOT NULL,
  `followers_count` BIGINT(20) DEFAULT NULL,
  `fullname` VARCHAR(255) DEFAULT NULL,
  `profilePicUrl` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

InnoDB

## Вендоры

-https://genderize.io/ - решение для получения пола по имени
-http://www.geonames.org/ - для определения местоположения по координатам