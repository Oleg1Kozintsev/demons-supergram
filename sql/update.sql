ALTER TABLE `cache_stats_accounts`
	DROP COLUMN `top_photo`,
	DROP COLUMN `last_media_pk`,
	DROP COLUMN `last_media_url`,
	DROP COLUMN `last_media_caption`,
	DROP COLUMN `last_media_timestamp`,
	DROP COLUMN `last_media_code`;


ALTER TABLE `stats_accounts`
  ADD COLUMN `is_404` TINYINT(4) NULL DEFAULT '0' AFTER `is_business`;
  
ALTER TABLE `statistics_likers`
  ADD COLUMN `username` VARCHAR(50) NOT NULL AFTER `media_id`;

ALTER TABLE `statistics_likers`
  CHANGE COLUMN `media_id` `media_id` VARCHAR(25) NOT NULL AFTER `user_id`;

ALTER TABLE `stat_post`
  CHANGE COLUMN `pk` `pk` VARCHAR(25) NULL DEFAULT NULL AFTER `taken_at`;

-- -----------------------------------------------------
-- Table `supergram`.`bot`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `supergram`.`bot` ;

CREATE TABLE IF NOT EXISTS `supergram`.`bot` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(15) NOT NULL,
  `password` VARCHAR(10) NOT NULL,
  `email` VARCHAR(40) NOT NULL,
  `email_pass` VARCHAR(10) NOT NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  `is_check` TINYINT NOT NULL DEFAULT 0,
  `is_login` TINYINT NOT NULL DEFAULT 0,
  `phone` VARCHAR(20) NULL,
  `idproxy` INT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `username_UNIQUE` ON `supergram`.`bot` (`username` ASC);

CREATE UNIQUE INDEX `email_UNIQUE` ON `supergram`.`bot` (`email` ASC);

CREATE INDEX `fk_bot_proxy1_idx` ON `supergram`.`bot` (`idproxy` ASC);