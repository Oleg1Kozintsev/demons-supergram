USE supergram;

CREATE TABLE `cache_new_accounts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `is_sync` tinyint(4) NOT NULL DEFAULT '0',
  `is_error` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `cache_stats_accounts`;

CREATE TABLE `cache_stats_accounts` (
  `id` bigint(20) NOT NULL,
  `username` varchar(45) NOT NULL,
  `followers_count` bigint(20) DEFAULT NULL,
  `followings_count` bigint(20) DEFAULT NULL,
  `media_count` bigint(20) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `biography` varchar(255) DEFAULT NULL,
  `birthday` varchar(45) DEFAULT NULL,
  `latitude` varchar(45) DEFAULT NULL,
  `longitude` varchar(45) DEFAULT NULL,
  `publicphonenumber` varchar(45) DEFAULT NULL,
  `socialcontext` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `profilePicUrl` varchar(255) DEFAULT NULL,
  `pk` bigint(20) DEFAULT NULL,
  `top_photo` mediumtext,
  `is_sync` tinyint(4) NOT NULL DEFAULT '0',
  `is_error` tinyint(4) NOT NULL DEFAULT '0',
  `is_do` tinyint(4) NOT NULL DEFAULT '1',
  `is_show` tinyint(4) NOT NULL DEFAULT '0',
  `like_avg` bigint(20) DEFAULT '0',
  `comment_avg` bigint(20) DEFAULT '0',
  `rating` bigint(20) DEFAULT '0',
  `er` float DEFAULT '0',
  `lr` float DEFAULT '0',
  `tr` float DEFAULT '0',
  `last_media_pk` bigint(20) DEFAULT '0',
  `last_media_url` varchar(255) DEFAULT '0',
  `last_media_caption` varchar(1024) DEFAULT '0',
  `last_media_timestamp` int(11) DEFAULT '0',
  `last_media_code` varchar(25) DEFAULT '0',
  `country` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `countryCode` varchar(10) DEFAULT NULL,
  `req_auth` tinyint(4) DEFAULT '0',
  `is_business` tinyint(4) DEFAULT '0',
  `is_404` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `pk` (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO cache_stats_accounts(id, username, followers_count, followings_count, media_count, fullname, gender, email, biography, birthday,
latitude, longitude, publicphonenumber, socialcontext, created_at, updated_at, profilePicUrl, pk, top_photo, is_sync, is_error, is_do, is_show, like_avg, 
comment_avg, rating, er, lr, tr, last_media_pk, last_media_url, last_media_caption, last_media_timestamp, last_media_code, country, city, countryCode,
req_auth, is_business, is_404)
SELECT id, username, followers_count, followings_count, media_count, fullname, gender, email, biography, birthday,
latitude, longitude, publicphonenumber, socialcontext, created_at, updated_at, profilePicUrl, pk, top_photo, is_sync, is_error, is_do, is_show, like_avg, 
comment_avg, rating, er, lr, tr, last_media_pk, last_media_url, last_media_caption, last_media_timestamp, last_media_code, country, city, countryCode,
req_auth, is_business, is_404 FROM stats_accounts
WHERE is_do = 1 AND pk is not null