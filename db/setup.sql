/*
SQLyog Community v12.4.3 (64 bit)
MySQL - 5.5.52-MariaDB-cll-lve : Database - supergram
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`supergram` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `supergram`;

/*Table structure for table `instagram` */

DROP TABLE IF EXISTS `instagram`;

CREATE TABLE `instagram` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `idproxy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_instagram_account_user1_idx` (`user_id`),
  KEY `idproxy` (`idproxy`),
  CONSTRAINT `FK_instagram_proxy` FOREIGN KEY (`idproxy`) REFERENCES `proxy` (`idproxy`),
  CONSTRAINT `FK_instagram_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `instagram` */

insert  into `instagram`(`id`,`username`,`fullname`,`password`,`foto`,`user_id`,`idproxy`) values 
(1,'dunsolomon','Соломон Дун','c63255bb2f','https://instagram.fhen1-1.fna.fbcdn.net/t51.2885-19/s150x150/17494643_1810864972570235_7090503947814699008_a.jpg',4,1),
(4,'worldofbeautyful','Wonderful World','DYzjWFYKrJUv','https://instagram.fhen1-1.fna.fbcdn.net/t51.2885-19/s150x150/19765098_326107837828752_8264105457958780928_a.jpg',4,2);

/*Table structure for table `invite` */

DROP TABLE IF EXISTS `invite`;

CREATE TABLE `invite` (
  `invite_id` varchar(50) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`invite_id`),
  KEY `fk_invitee_user1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `invite` */

insert  into `invite`(`invite_id`,`user_id`,`created_at`,`status`) values 
('fmHV6jyfuSBXnIGT7rePZ9Fhp',NULL,1502176544,10);

/*Table structure for table `proxy` */

DROP TABLE IF EXISTS `proxy`;

CREATE TABLE `proxy` (
  `idproxy` int(11) NOT NULL AUTO_INCREMENT,
  `iporhost` varchar(255) DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  `login` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `host_type` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idproxy`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `proxy` */

insert  into `proxy`(`idproxy`,`iporhost`,`port`,`login`,`password`,`host_type`) values 
(1,'185.22.175.225',3128,'38yjmwri','6fb7xdl5','http'),
(2,'185.22.175.225',3128,'tvz7i38d','jkcns6q4','http');

/*Table structure for table `task` */

DROP TABLE IF EXISTS `task`;

CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT '0',
  `task_time` int(11) NOT NULL,
  `start` int(11) NOT NULL,
  `end` int(11) NOT NULL,
  `task` varchar(255) NOT NULL,
  `launched` tinyint(4) NOT NULL,
  `сompleted` tinyint(4) NOT NULL,
  `id_instagram` int(11) NOT NULL,
  `id_case` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_task_task_case1_idx` (`id_case`),
  KEY `fk_task_instagram_account1_idx` (`id_instagram`),
  CONSTRAINT `FK_task_instagram` FOREIGN KEY (`id_instagram`) REFERENCES `instagram` (`id`),
  CONSTRAINT `FK_task_task_case` FOREIGN KEY (`id_case`) REFERENCES `task_case` (`id_case`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

/*Data for the table `task` */

insert  into `task`(`id`,`title`,`task_time`,`start`,`end`,`task`,`launched`,`сompleted`,`id_instagram`,`id_case`) values 
(30,'Upload photo',1496304200,1496304000,1496304300,'{\"photo\":\"5935a95cd7449.jpg\",\"caption\":\"111\"}',0,0,1,1),
(31,'Upload photo',1496860966,1496860200,1496861100,'{\"photo\":\"5935a967c460a.jpg\",\"caption\":\"111\"}',0,0,1,1),
(32,'Upload photo',1496923222,1496923200,1496925000,'{\"photo\":\"59369ae3adb1a.jpg\",\"caption\":\"parsi\"}',0,0,1,1);

/*Table structure for table `task_case` */

DROP TABLE IF EXISTS `task_case`;

CREATE TABLE `task_case` (
  `id_case` int(11) NOT NULL AUTO_INCREMENT,
  `casename` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_case`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `task_case` */

insert  into `task_case`(`id_case`,`casename`) values 
(1,'upload_image');

/*Table structure for table `token` */

DROP TABLE IF EXISTS `token`;

CREATE TABLE `token` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(50) NOT NULL,
  `expired_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`),
  KEY `idx-token-user_id` (`user_id`),
  CONSTRAINT `idx-token-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `token` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `auth_key` varchar(32) DEFAULT NULL,
  `password_hash` varchar(60) NOT NULL,
  `password_reset_token` varchar(100) DEFAULT NULL,
  `email_confirm_token` varchar(32) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `role` smallint(6) NOT NULL DEFAULT '10',
  `status` smallint(6) NOT NULL DEFAULT '10',
  `access_token` varchar(100) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `timezone` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email_confirm_token`,`email`,`role`,`status`,`access_token`,`created_at`,`updated_at`,`timezone`) values 
(3,'admin_old','VcXClZ8bKM_If7EXVW1ZqX5_qeXzhhop','$2y$13$fh2biNybADskZix0TItqE.Fcw5Nb9.6z0q6.sexaTSv5/urRIefXq',NULL,'sRi2_4b2zO6zc8gyoQCNyAL_nNrJik6P','o.kozintsev@gmail.com',10,0,NULL,1502176544,1502176544,NULL),
(4,'admin','LwrJjkOj6P44QBi0OXRqdsvk88_8lVYm','$2y$13$TtHLy1zZY3VGZAcZo05WnuDyS7nlc5qEUHreTODZCbTu2zaEFbPDK',NULL,'_R-7-YoMacgYC08fPYg-0E1t5mFZ87Tk','trmp@inbox.ru',10,1,NULL,1506497131,1506497131,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
