/*
SQLyog Community v12.5.0 (64 bit)
MySQL - 5.5.52-MariaDB-cll-lve : Database - supergram
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`supergram` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `supergram`;

/*Table structure for table `audience` */

DROP TABLE IF EXISTS `audience`;

CREATE TABLE `audience` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `accounts_id` bigint(20) NOT NULL,
  `type` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` bigint(20) NOT NULL,
  `name_ru` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

/*Table structure for table `cache_audience` */

DROP TABLE IF EXISTS `cache_audience`;

CREATE TABLE `cache_audience` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `data` varchar(255) DEFAULT NULL,
  `accounts_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Table structure for table `cache_audience_reach` */

DROP TABLE IF EXISTS `cache_audience_reach`;

CREATE TABLE `cache_audience_reach` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `high` bigint(20) DEFAULT NULL,
  `middle` bigint(20) DEFAULT NULL,
  `low` bigint(20) DEFAULT NULL,
  `accounts_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Table structure for table `cache_new_accounts` */

DROP TABLE IF EXISTS `cache_new_accounts`;

CREATE TABLE `cache_new_accounts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `is_sync` tinyint(4) NOT NULL DEFAULT '0',
  `is_error` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

/*Table structure for table `cache_stats_accounts` */

DROP TABLE IF EXISTS `cache_stats_accounts`;

CREATE TABLE `cache_stats_accounts` (
  `id` bigint(20) NOT NULL,
  `username` varchar(45) NOT NULL,
  `followers_count` bigint(20) DEFAULT NULL,
  `followings_count` bigint(20) DEFAULT NULL,
  `media_count` bigint(20) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `biography` varchar(255) DEFAULT NULL,
  `birthday` varchar(45) DEFAULT NULL,
  `latitude` varchar(45) DEFAULT NULL,
  `longitude` varchar(45) DEFAULT NULL,
  `publicphonenumber` varchar(45) DEFAULT NULL,
  `socialcontext` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `profilePicUrl` varchar(255) DEFAULT NULL,
  `pk` bigint(20) DEFAULT NULL,
  `is_sync` tinyint(4) NOT NULL DEFAULT '0',
  `is_error` tinyint(4) NOT NULL DEFAULT '0',
  `is_do` tinyint(4) NOT NULL DEFAULT '1',
  `is_show` tinyint(4) NOT NULL DEFAULT '0',
  `like_avg` bigint(20) DEFAULT '0',
  `comment_avg` bigint(20) DEFAULT '0',
  `rating` bigint(20) DEFAULT '0',
  `er` float DEFAULT '0',
  `lr` float DEFAULT '0',
  `lr_followers` float DEFAULT '0',
  `tr` float DEFAULT '0',
  `last_media_pk` bigint(20) DEFAULT '0',
  `last_media_url` varchar(255) DEFAULT '0',
  `last_media_caption` varchar(1024) DEFAULT '0',
  `last_media_timestamp` int(11) DEFAULT '0',
  `last_media_code` varchar(25) DEFAULT '0',
  `country` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `countryCode` varchar(10) DEFAULT NULL,
  `req_auth` tinyint(4) DEFAULT '0',
  `is_business` tinyint(4) DEFAULT '0',
  `is_404` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `pk` (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `cache_top_followers` */

DROP TABLE IF EXISTS `cache_top_followers`;

CREATE TABLE `cache_top_followers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `follower_id` bigint(20) NOT NULL,
  `username` varchar(45) NOT NULL,
  `followers_count` bigint(20) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `profilePicUrl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=651 DEFAULT CHARSET=utf8;

/*Table structure for table `processes` */

DROP TABLE IF EXISTS `processes`;

CREATE TABLE `processes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `status` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `service_log` */

DROP TABLE IF EXISTS `service_log`;

CREATE TABLE `service_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `stat_post` */

DROP TABLE IF EXISTS `stat_post`;

CREATE TABLE `stat_post` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instagram_id` bigint(20) NOT NULL,
  `url` varchar(200) DEFAULT NULL,
  `caption` text,
  `code` varchar(45) DEFAULT NULL,
  `taken_at` int(11) DEFAULT NULL,
  `pk` bigint(20) DEFAULT NULL,
  `like_count` int(11) DEFAULT '0',
  `comment_count` int(11) DEFAULT '0',
  `location` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pk` (`pk`),
  KEY `fk_stat_post_stats_accounts1_idx` (`instagram_id`),
  CONSTRAINT `FK_stat_post_stats_accounts` FOREIGN KEY (`instagram_id`) REFERENCES `stats_accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78076 DEFAULT CHARSET=utf8;

/*Table structure for table `statistics_comments` */

DROP TABLE IF EXISTS `statistics_comments`;

CREATE TABLE `statistics_comments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `media_id` bigint(20) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_statistics_comments_statistic_instagram_account1_idx` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1946 DEFAULT CHARSET=utf8;

/*Table structure for table `statistics_likers` */

DROP TABLE IF EXISTS `statistics_likers`;

CREATE TABLE `statistics_likers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `media_id` bigint(20) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_statistic_like_statistic_instagram_account1_idx` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=61758 DEFAULT CHARSET=utf8;

/*Table structure for table `stats_accounts` */

DROP TABLE IF EXISTS `stats_accounts`;

CREATE TABLE `stats_accounts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `followers_count` bigint(20) DEFAULT NULL,
  `followings_count` bigint(20) DEFAULT NULL,
  `media_count` bigint(20) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `biography` varchar(255) DEFAULT NULL,
  `birthday` varchar(45) DEFAULT NULL,
  `latitude` varchar(45) DEFAULT NULL,
  `longitude` varchar(45) DEFAULT NULL,
  `publicphonenumber` varchar(45) DEFAULT NULL,
  `socialcontext` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `profilePicUrl` varchar(255) DEFAULT NULL,
  `pk` bigint(20) NOT NULL,
  `is_sync` tinyint(4) NOT NULL DEFAULT '0',
  `is_error` tinyint(4) NOT NULL DEFAULT '0',
  `is_do` tinyint(4) NOT NULL DEFAULT '1',
  `is_show` tinyint(4) NOT NULL DEFAULT '0',
  `like_avg` bigint(20) DEFAULT '0',
  `comment_avg` bigint(20) DEFAULT '0',
  `rating` bigint(20) DEFAULT '0',
  `er` float DEFAULT '0',
  `lr` float DEFAULT '0',
  `lr_followers` float DEFAULT '0',
  `tr` float DEFAULT '0',
  `country` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `countryCode` varchar(10) DEFAULT NULL,
  `req_auth` tinyint(4) DEFAULT '0',
  `is_business` tinyint(4) DEFAULT '0',
  `is_404` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `pk` (`pk`)
) ENGINE=InnoDB AUTO_INCREMENT=9466466 DEFAULT CHARSET=utf8;

/*Table structure for table `stats_followers` */

DROP TABLE IF EXISTS `stats_followers`;

CREATE TABLE `stats_followers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `follower_id` bigint(20) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_statistic_followers_statistic_instagram_account1_idx` (`user_id`),
  KEY `fk_statistic_followers_statistic_instagram_account2_idx` (`follower_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9567987 DEFAULT CHARSET=utf8;

/*Table structure for table `stats_followings` */

DROP TABLE IF EXISTS `stats_followings`;

CREATE TABLE `stats_followings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `following_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_statistic_followings_statistic_instagram_account1_idx` (`user_id`),
  KEY `fk_statistic_followings_statistic_instagram_account2_idx` (`following_id`)
) ENGINE=InnoDB AUTO_INCREMENT=956 DEFAULT CHARSET=utf8;

/*Table structure for table `stats_likes_last_post` */

DROP TABLE IF EXISTS `stats_likes_last_post`;

CREATE TABLE `stats_likes_last_post` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instagram_id` bigint(20) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `like_count` int(11) DEFAULT NULL,
  `comment_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_time_statistic_statistic_instagram_account1_idx` (`instagram_id`)
) ENGINE=InnoDB AUTO_INCREMENT=380983 DEFAULT CHARSET=utf8;

/*Table structure for table `stats_timeline` */

DROP TABLE IF EXISTS `stats_timeline`;

CREATE TABLE `stats_timeline` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instagram_id` bigint(20) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `followers_count` bigint(20) DEFAULT NULL,
  `followings_count` bigint(20) DEFAULT NULL,
  `media_count` bigint(20) DEFAULT NULL,
  `like_avg` bigint(20) DEFAULT NULL,
  `comment_avg` bigint(20) DEFAULT NULL,
  `rating` bigint(20) DEFAULT NULL,
  `er` float DEFAULT NULL,
  `lr` float DEFAULT NULL,
  `tr` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_time_statistic_statistic_instagram_account1_idx` (`instagram_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39648 DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
