#!/usr/bin/php
<?php
/**
 * @author Oleg Kozintsev
 * @link
 * скрипт для получение geo данных
 */
//Игнорировать обрыв связи с браузером
ignore_user_abort(1);
//Время работы скрипта неограничено
set_time_limit(0);
date_default_timezone_set('UTC');

require_once("vendor/autoload.php");

use demonsThebloggers\Actions\geoAnalyzer;

$lat = 55.748914;
$lng = 37.604290;

// 54.300815, 48.329320

echo "lat = " . $lat . " lng = " . $lng . "\n";

$a = new geoAnalyzer();
try {
    $a->getPlace($lat, $lng);
} catch (\GuzzleHttp\Exception\GuzzleException $e) {
    echo "GuzzleException: " . $e->getMessage();
    exit(1);
}
$a->parseJson();

echo "Country: " . $a->getCountryName() . " City:" . $a->getCityName() . "\n";

echo "Finish \n";