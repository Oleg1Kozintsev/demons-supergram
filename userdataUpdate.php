<?php
/**
 * @author Oleg Kozintsev
 * @link
 * скрипт для обновление пользовательских данных взятых с web page
 */
//Игнорировать обрыв связи с браузером
ignore_user_abort(1);
//Время работы скрипта неограничено
set_time_limit(0);

require_once("vendor/autoload.php");
require_once("config/db.php");

use Medoo\Medoo;
use kozintsev\ALogger\Logger;


$logger = new Logger(__DIR__ . '/log/parse.log', \Psr\Log\LogLevel::INFO);

/**
 * @return array
 */
function getProxyList(): array
{
    echo "Get proxy list: \n";
    $array = [];
    try {
        $handle = fopen(__DIR__ . "/proxy.txt", 'r+');
        while (!feof($handle)) {
            $buffer = fgets($handle, 4096);
            $array [] = $buffer;
            echo $buffer;
        }
        fclose($handle);
    } catch (Exception $e) {
        echo 'Выброшено исключение: ', $e->getMessage(), "\n";
        $handle = null;
        return [];
    }
    return $array;
}

/**
 * @param Medoo $database
 * @param $arr
 * @param Logger $logger
 * @param $start
 * @param $finish
 * @param $proxyList
 * @return bool
 */
function SelectPagination(Medoo $database, $arr, Logger $logger, $start, $finish, $proxyList)
{
    $client = new GuzzleHttp\Client();

    $arr = array_merge($arr, ['LIMIT' => [$start, $finish]]);

    //print_r($database->info());

    $rows = $database->select("stats_accounts", [
        "username", "pk"
    ], $arr);

    $rows_count = count($rows);

    if ($rows_count === 0){
        $logger->error('rows_count = 0 Connect close.');
        return false;
    }

    $i = 0;
    $proxy_count = count($proxyList);

    foreach ($rows as $item) {
        $pk = $item['pk'];
        $url = 'http://i.instagram.com/api/v1/users/' . $pk . '/info/';
        if ($rows_count > $proxy_count) {
            if ($i > $proxy_count - 1) {
                $i = 0;
                //sleep(1);
            }
            $proxy = $proxyList[$i];
        } else {
            $proxy = $proxyList[$i];
        }

        try {
            // proxy format : http://username:password@192.168.16.1:10
            if (empty($proxyList)) {
                $res = $client->request('GET', $url);
            } else {
                $res = $client->request('GET', $url, [
                    'proxy' => $proxy
                ]);
            }

            $status = $res->getStatusCode();

            if ($status === 200) {
                $body = $res->getBody();
                $json = json_decode($body);
                if (! is_object($json)){
                    $logger->error('Json not decode as object.');
                    continue;
                }
                if ( ! property_exists($json, 'user')){
                    $logger->error('Property user not exists.');
                    $logger->debug("Body: \n" . $body);
                    continue;
                }
                if (! property_exists($json->user, 'follower_count'))
                {
                    $logger->error('Property follower_count not exists.');
                    $logger->debug("Body: \n" . $body);
                    continue;
                }
                $follower_count = $json->user->follower_count;
                $media_count = $json->user->media_count;
                $following_count = $json->user->following_count;
                $username = $json->user->username;

                if (empty($username)) {
                    $logger->error('Try update database but empty username.');
                    continue;
                }

                $date = new DateTime();
                $timestamp = $date->getTimestamp();

                $data = $database->update("stats_accounts", [
                    'followers_count' => $follower_count,
                    'followings_count' => $following_count,
                    'media_count' => $media_count,
                    'updated_at' => $timestamp
                ], [
                    'pk' => $pk
                ]);

                if ($data->rowCount() === 0){
                    $logger->error('User data not updated! ' . sprintf("username %s follower_count: %s media_count %s pk %s",
                            $username, $follower_count, $media_count, $pk));
                } else {
                    $logger->debug('User data updated ' . sprintf("username %s follower_count: %s media_count %s pk %s",
                            $username, $follower_count, $media_count, $pk));
                }
            }
        } catch (\Exception $e) {
            $logger->error('Exception: ' . $url . "\n" . 'Error message: ' . $e->getMessage());
            if ($e->getCode() === 404) {
                $logger->debug('Update status is_404 ... ');
                $date = new DateTime();
                $timestamp = $date->getTimestamp();
                $database->update("stats_accounts", [
                    'is_404' => 1,
					'updated_at' => $timestamp
                ], [
                    'pk' => $pk
                ]);
                $logger->info('Status update. Page not found 404. Continue the process. Next.');
            } else {
                sleep(120);
            }
        }
        $i++;
        //break; //- fot faster tests
        //if ($i == 3) break;
        // sleep(1);
    }
    return true;
}

$arr = [
    "is_sync" => 1,
    "is_error" => 0,
    "is_do" => 0,
    "is_404" => 0,
    "followers_count" => null
];

$proxyList = getProxyList();

// цикл для перезапуска подключения к БД в слечае ошибки
while (1){
    $is_error = false;

    $database = new Medoo([
        'database_type' => 'mysql',
        'database_name' => $db_name,
        'server' => $db_server,
        'username' => $db_user,
        'password' => $db_pass,
        'charset' => 'utf8',
    ]);

    $rows_count = $database->count("stats_accounts", $arr);

    if ($rows_count == 0) {
        echo "Rows count = 0 \n";
        return;
    }

    echo "Start! Accounts: " . $rows_count . "\n";

    $logger->info("Start! Accounts: " . $rows_count);

    //thread number, for example 1
    $t = 1;
    //thread count
    $t_count = 1;

    $f = 500;
    $c = round($rows_count / $f);
    $m = $rows_count % $f;
    $s = 0;
    for ($i = $t; $i <= $c; $i = $i + $t_count) {

        if ( ! SelectPagination($database, $arr, $logger, $s, $f, $proxyList)){
            $is_error = true;
            break;
        }
        $s = $f;
        $f = $i * 500;
        //break; //- fot faster tests
    }
    if ($m != 0 && ! $is_error) {
        if ( ! SelectPagination($database, $arr, $logger, $f, $f + $m, $proxyList)){
            $is_error = true;
        }
    }
    echo "Restart connect ... \n";
    $logger->info("Restart connect ...");
    if ( ! $is_error) break;
}

$logger->debug("Exit");

print "Exit";