<?php
/**
 * @author Oleg Kozintsev
 * @link
 * скрипт для ополучение пола пользователя на основе его имени с сервиса: genderize.io
 */
//Игнорировать обрыв связи с браузером
ignore_user_abort(1);
//Время работы скрипта неограничено
set_time_limit(0);

require_once("vendor/autoload.php");
require_once("config/db.php");

use Medoo\Medoo;
use kozintsev\ALogger\Logger;

$logger = new Logger(__DIR__ . '/log/parse-gender.log', \Psr\Log\LogLevel::DEBUG);


/**
 * @param Medoo $database
 * @param array $arr
 * @param Logger $logger
 * @param $start
 * @param $finish
 * @return bool
 */
function SelectPagination(Medoo $database, array $arr, Logger $logger, $start, $finish)
{
    $apikey = '';

    $client = new GuzzleHttp\Client();

    $arr = array_merge($arr, ['LIMIT' => [$start, $finish]]);

    $rows = $database->select("stats_accounts", [
        "username", "pk", "fullname"
    ], $arr);

    $rows_count = count($rows);

    if ($rows_count === 0) {
        $logger->error('rows_count = 0 Connect close.');
        return false;
    }

    $i = 0;

    foreach ($rows as $item) {
        $pk = $item['pk'];
        $username = $item['username'];
        $fullname = $item['fullname'];
        if (strlen($fullname) < 3)
        {
            $logger->debug('Full name is short');
            continue;
        }
        if ($i > 900) {
            $logger->info('Rate limit > 900');
            return false;
        }
        //удаляем спецсимволы
        $fullname = str_replace('#', '', $fullname);
        //$fullname_new = preg_replace('%[^a-zа-я\d\s,.]%i', '', $fullname);
        // разбиваен на слова
        $keywords = preg_split("/[\s,.&]+/", $fullname);
        foreach ($keywords as $word) {
            if (strlen($word) < 3){
                $logger->debug('Word is short. Next');
                continue;
            }
            $url = empty($apikey) ? 'https://api.genderize.io/?name=' . $word : 'https://api.genderize.io/?name=' . $word . '?apikey=' . $apikey;

            try {
                $res = $client->request('GET', $url);
                $logger->debug("Request number: " . $i);
                $i++;

                $status = $res->getStatusCode();

                if ($status === 200) {
                    $body = $res->getBody();
                    $json = json_decode($body);
                    // {"name":"kim","gender":"female","probability":0.88,"count":3561}
                    // {"name":"oleg","gender":"male","probability":0.88,"count":3561}
                    if (!is_object($json)) {
                        $logger->error('Json not decode as object.');
                        continue;
                    }
                    if (!property_exists($json, 'gender')) {
                        $logger->error('Property gender not exists.');
                        $logger->debug("Body: \n" . $body);
                        continue;
                    }
                    $gender = $json->gender;
                    $name = $json->name;

                    if ($gender === null){
                        $logger->debug('Gender is null. Gender undefined');
                        $gender = "undefined";
                    }


                    $date = new DateTime();
                    $timestamp = $date->getTimestamp();

                    $data = $database->update("stats_accounts", [
                        'gender' => $gender,
                        'updated_at' => $timestamp
                    ], [
                        'pk' => $pk
                    ]);

                    if ($data->rowCount() === 0) {
                        $logger->error(sprintf('User data not updated! ' . sprintf("username %s fullname %s pk %s gender: %s",
                                $username, $fullname, $pk, $gender)));
                    } else {
                        $logger->debug(sprintf('User data updated. ' . sprintf("username %s  fullname %s pk %s gender: %s",
                                $username, $fullname, $pk, $gender)));
                    }
                    break;
                } else {
                    $logger->error('Status code: ' . $status);
                }
            } catch (\Exception $e) {
                $logger->error('Exception: ' . $url . "\n" . 'Error message: ' . $e->getMessage());
                if ($e->getCode() === 429) {
                    return false;
                }
            }
            //break; //- fot faster tests
            //if ($i == 3) break;
            sleep(10);
        }
    }
    return true;
}


$arr = [
    "is_sync" => 1,
    "is_error" => 0,
    "is_404" => 0,
    "gender" => null
];

$is_error = false;

$database = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $db_name,
    'server' => $db_server,
    'username' => $db_user,
    'password' => $db_pass,
    'charset' => 'utf8',
]);

$rows_count = $database->count("stats_accounts", $arr);

if ($rows_count == 0) {
    echo "Rows count = 0 \n";
    return;
}

echo "Start! Accounts: " . $rows_count . "\n";

$logger->info("Start! Accounts: " . $rows_count);

$f = 500;
$c = round($rows_count / $f);
$m = $rows_count % $f;
$s = 0;
for ($i = 1; $i <= $c; $i++) {
    if (!SelectPagination($database, $arr, $logger, $s, $f)) {
        $is_error = true;
        break;
    }
    $s = $f;
    $f = $i * $f;
    //break; //- fot faster tests
}
if ($m != 0 && !$is_error) {
    if (!SelectPagination($database, $arr, $logger, $f, $f + $m)) {
        $is_error = true;
    }
}


$logger->debug("Exit");

print "Exit";