#!/usr/bin/php
<?php
/**
 * @author Oleg Kozintsev
 * @link
 * служба для анализа добавленных пользователем пользователей в Task
 */
//Игнорировать обрыв связи с браузером
ignore_user_abort(1);
//Время работы скрипта неограничено
set_time_limit(0);
date_default_timezone_set('UTC');

require_once("vendor/autoload.php");
require_once("utilities/Logger.php");
require_once("models/Proxy.php");
require_once("models/Post.php");
require_once("actions/ratingCalc.php");
require_once("config/db.php");
require_once("config/main.php");
require_once("config/user-worldofbeautyful.php");

use Medoo\Medoo;
use demonsThebloggers\Actions\accountAnalyzer;
use demonsThebloggers\Models\Proxy;

$cron = True;

// Initialize
$database = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $db_name,
    'server' => 'localhost',
    'username' => $db_user,
    'password' => $db_pass,
    'charset' => 'utf8',
]);


function UpdateInstagram($id, $info, Medoo $database){
    $database->update('instagram', [
        'fullname' => $info['fullname'],
        'foto' => $info['profilePicUrl'],
    ], [
        'id' => $id
    ]);
}

function RunTasks($username, $password, $proxyCfg, Medoo $database, $log_file){
    $rows = $database->select("instagram", [
        "id", "username",
    ], [
        "foto" => null
    ] );

    if (count($rows) == 0) {
		echo "Rows count = 0 \n";
		return;
	}

    $logger = new Logger($log_file, true);

    $ig = new \InstagramAPI\Instagram(true, false);
    try {
        if (isset($proxyCfg) && $proxyCfg != [])
        {
            $proxy = new Proxy();
            $proxy->config($proxyCfg);
            $ig->setProxy($proxy->getUrl());
        }
        $ig->login($username, $password);
    } catch (\Exception $e) {
        echo 'Something went wrong: '.$e->getMessage()."\n";
        $logger->error('Something went wrong: '.$e->getMessage());
        exit(0);
    }

    foreach ($rows as $item) {
        // 1 поискать в базе статистики
        $analyzer = new accountAnalyzer($database, $ig, $item['username'], '', $log_file);
        if (($info = $analyzer->SearchInStatistic()) != null)
        {
            UpdateInstagram($item['id'], $info, $database);
            continue;
        } else {
            // добавляем в статистику получаем информацию от туда
            if ($analyzer->SearchAccount()){
                $analyzer->GetAccountInfo();
                if (($info = $analyzer->SearchInStatistic()) != null){
                    UpdateInstagram($item['id'], $info, $database);
                    continue;
                } else {
                    $logger->error('Instagram update search in statistic error!');
                }
            } else {
                $analyzer->setAccountError();
            }
        }
        sleep(5);
    }
}



if ($cron) {
    echo "Script run! \n";
    RunTasks($username, $password, $proxyCfg, $database, $log_file);
} else {
    echo "Demon run! \n";
    // Чтобы программа работала постоянно, она просто должна постоянно работать ;)
    while (1) {
        // Тут будет располагаться код Демона
        RunTasks($username, $password, $proxyCfg, $database, $log_file);
        // Время сна Демона между итерациями (зависит от потребностей системы)
        sleep(10);
    }
    print "Exit";
    exit(1);
}